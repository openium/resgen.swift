//
//  FontName.swift
//  
//
//  Created by Thibaut Schmitt on 29/08/2022.
//

import Foundation

typealias FontName = String

extension FontName {
    var fontNameSanitize: String {
        self.removeCharacters(from: "[]+-_")
    }
    
    func getProperty(isStatic: Bool, isSwiftUI: Bool) -> String {
        if isSwiftUI {
            if isStatic {
                return """
                    static let \(fontNameSanitize): ((_ size: CGFloat) -> Font) = { size in
                        Font.custom(FontName.\(fontNameSanitize).rawValue, size: size)
                    }
                """
            }
            return """
                func \(fontNameSanitize)(withSize size: CGFloat) -> Font {
                    Font.custom(FontName.\(fontNameSanitize).rawValue, size: size)
                }
            """
        }
        // UIKit
        if isStatic {
            return """
                static let \(fontNameSanitize): ((_ size: CGFloat) -> UIFont) = { size in
                    UIFont(name: FontName.\(fontNameSanitize).rawValue, size: size)!
                }
            """
        }
        return """
            func \(fontNameSanitize)(withSize size: CGFloat) -> UIFont {
                UIFont(name: FontName.\(fontNameSanitize).rawValue, size: size)!
            }
        """
    }
}
