//
//  Fonts.swift
//  
//
//  Created by Thibaut Schmitt on 13/12/2021.
//

import ToolCore
import Foundation
import ArgumentParser

struct Fonts: ParsableCommand {
    
    // MARK: - CommandConfiguration
    
    static var configuration = CommandConfiguration(
        abstract: "A utility to generate an helpful etension to access your custom font from code and also Info.plist UIAppsFont content.",
        version: ResgenSwiftVersion
    )
    
    // MARK: - Static
    
    static let toolName = "Fonts"
    static let defaultExtensionName = "Font"
    static let defaultExtensionNameUIKit = "UIFont"
    
    // MARK: - Command Options
    
    @OptionGroup var options: FontsOptions
    
    // MARK: - Run
    
    public func run() throws {
        print("[\(Self.toolName)] Starting fonts generation")
        print("[\(Self.toolName)] Will use inputFile \(options.inputFile) to generate fonts")
        
        // Check requirements
        guard checkRequirements() else { return }
        
        print("[\(Self.toolName)] Will generate fonts")
        
        // Get fonts to generate
        let fontsToGenerate = FontFileParser.parse(options.inputFile)
        
        // Get real font names
        let inputFolder = URL(fileURLWithPath: options.inputFile).deletingLastPathComponent().relativePath
        let fontsNames = FontsToolHelper.getFontPostScriptName(for: fontsToGenerate,
                                                              inputFolder: inputFolder)
        
        // Generate extension
        FontExtensionGenerator.writeExtensionFile(fontsNames: fontsNames,
                                                  staticVar: options.staticMembers,
                                                  extensionName: options.extensionName,
                                                  extensionFilePath: options.extensionFilePath,
                                                  isSwiftUI: true)
        
        FontExtensionGenerator.writeExtensionFile(fontsNames: fontsNames,
                                                  staticVar: options.staticMembers,
                                                  extensionName: options.extensionNameUIKit,
                                                  extensionFilePath: options.extensionFilePathUIKit,
                                                  isSwiftUI: false)
        
        print("Info.plist has been updated with:")
        print("\(FontPlistGenerator.generatePlistUIAppsFontContent(for: fontsNames, infoPlistPaths: options.infoPlistPaths))")
        
        print("[\(Self.toolName)] Fonts generated")
    }
    
    // MARK: - Requirements
    
    private func checkRequirements() -> Bool {
        let fileManager = FileManager()
        
        // Check input file exists
        guard fileManager.fileExists(atPath: options.inputFile) else {
            let error = FontsToolError.fileNotExists(options.inputFile)
            print(error.description)
            Fonts.exit(withError: error)
        }
        
        // Extension for UIKit and SwiftUI should have different name
        guard options.extensionName != options.extensionNameUIKit else {
            let error = FontsToolError.extensionNamesCollision(options.extensionName)
            print(error.description)
            Fonts.exit(withError: error)
        }
        
        // Check if needed to regenerate
        guard GeneratorChecker.shouldGenerate(force: options.forceGeneration,
                                              inputFilePath: options.inputFile,
                                              extensionFilePath: options.extensionFilePath) else {
            print("[\(Self.toolName)] Fonts are already up to date :) ")
            return false
        }
        
        return true
    }
}
