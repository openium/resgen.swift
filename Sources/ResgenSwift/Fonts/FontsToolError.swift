//
//  FontsToolError.swift
//  
//
//  Created by Thibaut Schmitt on 13/12/2021.
//

import Foundation

enum FontsToolError: Error {
    case extensionNamesCollision(String)
    case fcScan(String, Int32, String?)
    case inputFolderNotFound(String)
    case fileNotExists(String)
    case writeExtension(String, String)
    
    var description: String {
        switch self {
        case .extensionNamesCollision(let extensionName):
            return "error: [\(Fonts.toolName)] Error on extension names, extension name and SwiftUI extension name should be different (\(extensionName) is used on both)"
            
        case .fcScan(let path, let code, let output):
            return "error: [\(Fonts.toolName)] Error while getting fontName (fc-scan --format %{postscriptname} \(path). fc-scan exit with \(code) and output is: \(output ?? "no output")"
            
        case .inputFolderNotFound(let inputFolder):
            return "error: [\(Fonts.toolName)] Input folder not found: \(inputFolder)"
            
        case .fileNotExists(let filename):
            return "error: [\(Fonts.toolName)] File \(filename) does not exists"
            
        case .writeExtension(let filename, let info):
            return "error: [\(Fonts.toolName)] An error occured while writing extension in \(filename): \(info)"
        }
    }
}
