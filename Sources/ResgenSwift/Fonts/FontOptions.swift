//
//  FontsOptions.swift
//  
//
//  Created by Thibaut Schmitt on 17/01/2022.
//

import Foundation
import ArgumentParser

struct FontsOptions: ParsableArguments {
    @Flag(name: [.customShort("f"), .customShort("F")], help: "Should force generation")
    var forceGeneration = false
    
    @Argument(help: "Input files where fonts ared defined.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var inputFile: String
    
    @Option(help: "Path where to generate the extension.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var extensionOutputPath: String
    
    @Option(help: "Tell if it will generate static properties or methods")
    var staticMembers: Bool = false
    
    @Option(help: "Extension name. If not specified, it will generate an Font extension.")
    var extensionName: String = Fonts.defaultExtensionName
    
    @Option(help: "Extension name. If not specified, it will generate an UIFont extension.")
    var extensionNameUIKit: String = Fonts.defaultExtensionNameUIKit
    
    @Option(help: "Extension suffix. Ex: MyApp, it will generate {extensionName}+FontsMyApp.swift")
    var extensionSuffix: String = ""
    
    @Option(name: .customLong("info-plist-paths"), help: "Info.plist paths (array). Will be used to update UIAppFonts content")
    fileprivate var infoPlistPathsRaw: String = ""
}

// MARK: - Computed var

extension FontsOptions {
    
    // MARK: - SwiftUI
    
    var extensionFileName: String {
        if extensionSuffix.isEmpty == false {
            return "\(extensionName)+\(extensionSuffix).swift"
        }
        return "\(extensionName).swift"
    }
    
    var extensionFilePath: String {
        "\(extensionOutputPath)/\(extensionFileName)"
    }
    
    // MARK: - UIKit
    
    var extensionFileNameUIKit: String {
        if extensionSuffix.isEmpty == false {
            return "\(extensionNameUIKit)+\(extensionSuffix).swift"
        }
        return "\(extensionNameUIKit).swift"
    }
    
    var extensionFilePathUIKit: String {
        "\(extensionOutputPath)/\(extensionFileNameUIKit)"
    }

    // MARK: - 

    var infoPlistPaths: [String] {
        infoPlistPathsRaw
            .split(separator: " ")
            .map { String($0) }
    }
}
