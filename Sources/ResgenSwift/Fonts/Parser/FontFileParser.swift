//
//  FontFileParser.swift
//  
//
//  Created by Thibaut Schmitt on 29/08/2022.
//

import Foundation

class FontFileParser {
    static func parse(_ inputFile: String) -> [String] {
        let inputFileContent = try! String(contentsOfFile: inputFile,
                                           encoding: .utf8)
        return inputFileContent.components(separatedBy: CharacterSet.newlines)
    }
}
