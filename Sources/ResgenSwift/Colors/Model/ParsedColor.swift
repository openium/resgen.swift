//
//  ParsedColor.swift
//  
//
//  Created by Thibaut Schmitt on 20/12/2021.
//

import Foundation

struct ParsedColor {
    let name: String
    let light: String
    let dark: String
    
    // Generate Contents.json content
    func contentsJSON() -> String {
        let lightARGB = light.colorComponent()
        let darkARGB = dark.colorComponent()
        
        let allComponents = [
            lightARGB.alpha, lightARGB.red, lightARGB.green, lightARGB.blue,
            darkARGB.alpha, darkARGB.red, darkARGB.green, darkARGB.blue
        ].map {
            $0.isEmpty
        }
        
        guard allComponents.contains(true) == false else {
            let error = ColorsToolError.badColorDefinition(light, dark)
            print(error.description)
            Colors.exit(withError: error)
        }
        
        return """
        {
            "colors": [
                {
                    "color": {
                        "color-space": "srgb",
                        "components": {
                            "alpha": "0x\(lightARGB.alpha)",
                            "blue": "0x\(lightARGB.blue)",
                            "green": "0x\(lightARGB.green)",
                            "red": "0x\(lightARGB.red)",
                        }
                    },
                    "idiom": "universal"
                },
                {
                    "appearances": [
                        {
                            "appearance": "luminosity",
                            "value": "dark"
                        }
                    ],
                    "color": {
                        "color-space": "srgb",
                        "components": {
                            "alpha": "0x\(darkARGB.alpha)",
                            "blue": "0x\(darkARGB.blue)",
                            "green": "0x\(darkARGB.green)",
                            "red": "0x\(darkARGB.red)",
                        }
                    },
                    "idiom": "universal"
                }
            ],
            "info": {
                "author": "xcode",
                "version": 1
            }
        }
        """
    }
    
    // MARK: - UIKit
    
    func getColorProperty(isStatic: Bool, isSwiftUI: Bool) -> String {
        if isSwiftUI {
            return """
                /// Color \(name) is \(light) (light) or \(dark) (dark)"
                \(isStatic ? "static " : "")var \(name): Color {
                    Color("\(name)")
                }
            """
        }
        return """
            /// Color \(name) is \(light) (light) or \(dark) (dark)"
            \(isStatic ? "static " : "@objc ")var \(name): UIColor {
                UIColor(named: "\(name)")!
            }
        """
    }
}
