//
//  ColorStyle.swift
//  
//
//  Created by Thibaut Schmitt on 29/08/2022.
//

import Foundation
import ArgumentParser

enum ColorStyle: String, Decodable, ExpressibleByArgument {
    case light
    case all
    
    static var allValueStrings: [String] {
        [
            Self.light.rawValue,
            Self.all.rawValue
        ]
    }
}
