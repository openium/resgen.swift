//
//  ColorsToolOptions.swift
//  
//
//  Created by Thibaut Schmitt on 17/01/2022.
//

import Foundation
import ArgumentParser

struct ColorsToolOptions: ParsableArguments {
    @Flag(name: [.customShort("f"), .customShort("F")], help: "Should force generation")
    var forceGeneration = false
    
    @Argument(help: "Input files where colors ared defined.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var inputFile: String
    
    @Option(help: "Color style to generate: light for light colors only, or all for dark and light colors")
    var style: ColorStyle
    
    @Option(help: "Path of xcassets where to generate colors", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var xcassetsPath: String
    
    @Option(help: "Path where to generate the extension.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var extensionOutputPath: String
    
    @Option(help: "Tell if it will generate static properties or not")
    var staticMembers: Bool = false
    
    @Option(help: "Extension name. If not specified, it will generate an Color extension.")
    var extensionName: String = Colors.defaultExtensionName
    
    @Option(help: "SwiftUI Extension name. If not specified, it will generate an UIColor extension.")
    var extensionNameUIKit: String = Colors.defaultExtensionNameUIKit
    
    @Option(help: "Extension suffix. Ex: MyApp, it will generate {extensionName}+ColorsMyApp.swift")
    var extensionSuffix: String?
}

// MARK: - Computed var

extension ColorsToolOptions {
    
    // MARK: - SwiftUI
    
    var extensionFileName: String {
        if let extensionSuffix = extensionSuffix {
            return "\(extensionName)+\(extensionSuffix).swift"
        }
        return "\(extensionName).swift"
    }
    
    var extensionFilePath: String {
        "\(extensionOutputPath)/\(extensionFileName)"
    }
    
    // MARK: - UIKit
    
    var extensionFileNameUIKit: String {
        if let extensionSuffix = extensionSuffix {
            return "\(extensionNameUIKit)+\(extensionSuffix).swift"
        }
        return "\(extensionNameUIKit).swift"
    }
    
    var extensionFilePathUIKit: String {
        "\(extensionOutputPath)/\(extensionFileNameUIKit)"
    }
}
