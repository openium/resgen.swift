//
//  ColorsToolError.swift
//  
//
//  Created by Thibaut Schmitt on 20/12/2021.
//

import Foundation

enum ColorsToolError: Error {
    case extensionNamesCollision(String)
    case badFormat(String)
    case writeAsset(String)
    case createAssetFolder(String)
    case writeExtension(String, String)
    case fileNotExists(String)
    case badColorDefinition(String, String)
    case deleteExistingColors(String)
    
    var description: String {
        switch self {
        case .extensionNamesCollision(let extensionName):
            return "error: [\(Fonts.toolName)] Error on extension names, extension name and SwiftUI extension name should be different (\(extensionName) is used on both)"
            
        case .badFormat(let info):
            return "error: [\(Colors.toolName)] Bad line format: \(info). Accepted format are: colorName=\"#RGB/#ARGB\"; colorName \"#RGB/#ARGB\"; colorName \"#RGB/#ARGB\" \"#RGB/#ARGB\""
            
        case .writeAsset(let info):
            return "error: [\(Colors.toolName)] An error occured while writing color in Xcasset: \(info)"
         
        case .createAssetFolder(let assetsFolder):
            return "error: [\(Colors.toolName)] An error occured while creating colors folder `\(assetsFolder)`"
            
        case .writeExtension(let filename, let info):
            return "error: [\(Colors.toolName)] An error occured while writing extension in \(filename): \(info)"
            
        case .fileNotExists(let filename):
            return "error: [\(Colors.toolName)] File \(filename) does not exists"
            
        case .badColorDefinition(let lightColor, let darkColor):
            return "error: [\(Colors.toolName)] One of these two colors has invalid synthax: -\(lightColor)- or -\(darkColor)-"
            
        case .deleteExistingColors(let assetsFolder):
            return "error: [\(Colors.toolName)] An error occured while deleting colors folder `\(assetsFolder)`"
        }
    }
}
