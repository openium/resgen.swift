//
//  TagType.swift
//  
//
//  Created by Thibaut Schmitt on 08/12/2023.
//

import Foundation

extension AnalyticsDefinition {
    
    enum TagType {
        case screen
        case event
    }
}
