//
//  TargetType.swift
//  
//
//  Created by Thibaut Schmitt on 08/12/2023.
//

import Foundation

enum TrackerType: CaseIterable {
    case matomo
    case firebase

    var value: String {
        switch self {
        case .matomo:
            "matomo"
        case .firebase:
            "firebase"
        }
    }

    static func hasValidTarget(in targets: String) -> Bool {
        for tracker in Self.allCases where targets.contains(tracker.value) {
            return true
        }
        return false
    }
}
