//
//  AnalyticsCategory.swift
//  
//
//  Created by Loris Perret on 05/12/2023.
//

import Foundation

class AnalyticsCategory {
    let id: String // OnBoarding
    var definitions = [AnalyticsDefinition]()
    
    // MARK: - Init

    init(id: String) {
        self.id = id
    }
    
    // MARK: - Methods

    func hasOneOrMoreMatchingTags(tags: [String]) -> Bool {
        let allTags = definitions.flatMap { $0.tags }
        let allTagsSet = Set(allTags)
        
        let intersection = Set(tags).intersection(allTagsSet)
        if intersection.isEmpty {
            return false
        }
        return true
    }
}
