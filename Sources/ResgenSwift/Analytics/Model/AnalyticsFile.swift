//
//  AnalyticsFile.swift
//  
//
//  Created by Loris Perret on 06/12/2023.
//

import Foundation

struct AnalyticsFile: Codable {
    var categories: [AnalyticsCategoryDTO]
}

struct AnalyticsCategoryDTO: Codable {
    var id: String
    var screens: [AnalyticsDefinitionScreenDTO]?
    var events: [AnalyticsDefinitionEventDTO]?
}

struct AnalyticsDefinitionScreenDTO: Codable {
    var id: String
    var name: String
    var tags: String
    var comments: String?
    var parameters: [AnalyticsParameterDTO]?
    
    var path: String?
}

struct AnalyticsDefinitionEventDTO: Codable {
    var id: String
    var name: String
    var tags: String
    var comments: String?
    var parameters: [AnalyticsParameterDTO]?
    
    var category: String?
    var action: String?
}

struct AnalyticsParameterDTO: Codable {
    var name: String
    var type: String
    var replaceIn: String?
}
