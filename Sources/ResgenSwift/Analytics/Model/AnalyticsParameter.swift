//
//  AnalyticsParameter.swift
//
//
//  Created by Loris Perret on 06/12/2023.
//

import Foundation

class AnalyticsParameter {
    var name: String
    var type: String
    var replaceIn: [String] = []
    
    // MARK: - Init
    
    init(name: String, type: String) {
        self.name = name
        self.type = type
    }
}
