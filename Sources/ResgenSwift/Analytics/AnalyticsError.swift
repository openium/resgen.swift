//
//  AnalyticsError.swift
//
//
//  Created by Loris Perret on 11/12/2023.
//

import Foundation

enum AnalyticsError: Error {
    case noValidTracker(String)
    case fileNotExists(String)
    case missingElement(String)
    case invalidParameter(String)
    case parseFailed(String)
    case writeFile(String, String)
    
    var description: String {
        switch self {
        case .noValidTracker(let inputTargets):
            return "error: [\(Analytics.toolName)] '\(inputTargets)' ne contient aucun tracker valid"

        case .fileNotExists(let filename):
            return "error: [\(Analytics.toolName)] File \(filename) does not exists"
            
        case .missingElement(let element):
            return "error: [\(Analytics.toolName)] Missing \(element) for Matomo"
            
        case .invalidParameter(let reason):
            return "error: [\(Analytics.toolName)] Invalid parameter \(reason)"
            
        case .parseFailed(let baseError):
            return "error: [\(Analytics.toolName)] Parse input file failed: \(baseError)"

        case .writeFile(let subErrorDescription, let filename):
            return "error: [\(Analytics.toolName)] An error occured while writing content to \(filename): \(subErrorDescription)"
        }
    }
}
