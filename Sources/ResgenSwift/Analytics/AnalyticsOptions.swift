//
//  AnalyticsOptions.swift
//  
//
//  Created by Loris Perret on 08/12/2023.
//

import Foundation
import ArgumentParser

struct AnalyticsOptions: ParsableArguments {
    @Flag(name: [.customShort("f"), .customShort("F")], help: "Should force generation")
    var forceGeneration = false
    
    @Argument(help: "Input files where tags ared defined.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var inputFile: String
    
    @Option(help: "Target(s) analytics to generate. (\"matomo\" | \"firebase\")")
    var target: String
    
    @Option(help: "Path where to generate the extension.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var extensionOutputPath: String
    
    @Option(help: "Tell if it will generate static properties or not")
    var staticMembers: Bool = false
    
    @Option(help: "Extension name. If not specified, it will generate a Analytics extension.")
    var extensionName: String = Analytics.defaultExtensionName
    
    @Option(help: "Extension suffix. Ex: MyApp, it will generate {extensionName}+Analytics{extensionSuffix}.swift")
    var extensionSuffix: String?
}

// MARK: - Computed var

extension AnalyticsOptions {
    var extensionFileName: String {
        if let extensionSuffix = extensionSuffix {
            return "\(extensionName)+\(extensionSuffix).swift"
        }
        return "\(extensionName).swift"
    }
    
    var extensionFilePath: String {
        "\(extensionOutputPath)/\(extensionFileName)"
    }
}
