//
//  ConfigurationFileParser.swift
//  
//
//  Created by Thibaut Schmitt on 30/08/2022.
//

import Foundation
import Yams

class ConfigurationFileParser {
    static func parse(_ configurationFile: String) -> ConfigurationFile {
        guard let data = FileManager().contents(atPath: configurationFile) else {
            let error = GenerateError.fileNotExists(configurationFile)
            print(error.description)
            Generate.exit(withError: error)
        }
        
        do {
            return try YAMLDecoder().decode(ConfigurationFile.self, from: data)
        } catch {
            let error = GenerateError.invalidConfigurationFile(
                configurationFile,
                error.localizedDescription.description
            )
            print(error.description)
            Generate.exit(withError: error)
        }
    }
}
