//
//  StringExtensions.swift
//  
//
//  Created by Thibaut Schmitt on 31/08/2022.
//

import Foundation

extension String {
    
    func prependIfRelativePath(_ prependPath: String) -> String {
        // If path starts with "/", it's an absolute path
        if self.hasPrefix("/") {
            return self
        }
        return prependPath + self
    }
}
