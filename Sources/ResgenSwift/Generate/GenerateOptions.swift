//
//  GenerateOptions.swift
//  
//
//  Created by Thibaut Schmitt on 30/08/2022.
//

import Foundation
import ArgumentParser

struct GenerateOptions: ParsableArguments {
    @Flag(name: [.customShort("f"), .customShort("F")], help: "Should force generation")
    var forceGeneration = false
    
    @Argument(help: "Configuration file.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var configurationFile: String
    
    @Option(help: "Project directory. It will be added to every relative path (path that does not start with `/`",
            transform: {
        if $0.last == "/" {
            return $0
        }
        return $0 + "/"
    })
    var projectDirectory: String
}
