//
//  FontsConfiguration+Runnable.swift
//  
//
//  Created by Thibaut Schmitt on 30/08/2022.
//

import Foundation

extension FontsConfiguration: Runnable {
    func run(projectDirectory: String, force: Bool) {
        let args = getArguments(projectDirectory: projectDirectory, force: force)
        Fonts.main(args)
    }
    
    func getArguments(projectDirectory: String, force: Bool) -> [String] {
        var args = [String]()
        
        if force {
            args += ["-f"]
        }
        
        args += [
            inputFile.prependIfRelativePath(projectDirectory),
            "--extension-output-path",
            extensionOutputPath.prependIfRelativePath(projectDirectory),
            "--static-members",
            "\(staticMembersOptions)"
        ]
        
        if let extensionName = extensionName {
            args += [
                "--extension-name",
                extensionName
            ]
        }
        if let extensionNameUIKit = extensionNameUIKit {
            args += [
                "--extension-name-ui-kit",
                extensionNameUIKit
            ]
        }
        
        if let extensionSuffix = extensionSuffix {
            args += [
                "--extension-suffix",
                extensionSuffix
            ]
        }
        
        if let infoPlistPaths = infoPlistPaths {
            let adjustedPlistPaths = infoPlistPaths
                .split(separator: " ")
                .map { String($0).prependIfRelativePath(projectDirectory) }
                .joined(separator: " ")
            
            args += [
                "--info-plist-paths",
                adjustedPlistPaths
            ]
        }
        
        return args
    }
}
