//
//  TagsConfiguration+Runnable.swift
//  
//
//  Created by Thibaut Schmitt on 30/08/2022.
//

import Foundation

extension TagsConfiguration: Runnable {
    func run(projectDirectory: String, force: Bool) {
        var args = [String]()
        
        if force {
            args += ["-f"]
        }
        
        args += [
            inputFile.prependIfRelativePath(projectDirectory),
            "--lang",
            lang,
            "--extension-output-path",
            extensionOutputPath.prependIfRelativePath(projectDirectory),
            "--static-members",
            "\(staticMembersOptions)"
        ]
        
        if let extensionName = extensionName {
            args += [
                "--extension-name",
                extensionName
            ]
        }
        if let extensionSuffix = extensionSuffix {
            args += [
                "--extension-suffix",
                extensionSuffix
            ]
        }
        
        Tags.main(args)
    }
}
