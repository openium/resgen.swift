//
//  ColorsConfiguration+Runnable.swift
//  
//
//  Created by Thibaut Schmitt on 30/08/2022.
//

import Foundation

extension ColorsConfiguration: Runnable {
    func run(projectDirectory: String, force: Bool) {
        let args = getArguments(projectDirectory: projectDirectory, force: force)
        Colors.main(args)
    }
    
    func getArguments(projectDirectory: String, force: Bool) -> [String] {
        var args = [String]()
        
        if force {
            args += ["-f"]
        }
        
        args += [
            inputFile.prependIfRelativePath(projectDirectory),
            "--style",
            style,
            "--xcassets-path",
            xcassetsPath.prependIfRelativePath(projectDirectory),
            "--extension-output-path",
            extensionOutputPath.prependIfRelativePath(projectDirectory),
            "--static-members",
            "\(staticMembersOptions)"
        ]
        
        if let extensionName = extensionName {
            args += [
                "--extension-name",
                extensionName
            ]
        }
        if let extensionNameUIKit = extensionNameUIKit {
            args += [
                "--extension-name-ui-kit",
                extensionNameUIKit
            ]
        }
        if let extensionSuffix = extensionSuffix {
            args += [
                "--extension-suffix",
                extensionSuffix
            ]
        }
        
        return args
    }
}
