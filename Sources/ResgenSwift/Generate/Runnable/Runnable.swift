//
//  ShellCommandable.swift
//  
//
//  Created by Thibaut Schmitt on 30/08/2022.
//

import Foundation

protocol Runnable {
    func run(projectDirectory: String, force: Bool)
}
