//
//  StringsConfiguration+Runnable.swift
//  
//
//  Created by Thibaut Schmitt on 30/08/2022.
//

import Foundation

extension StringsConfiguration: Runnable {
    func run(projectDirectory: String, force: Bool) {
        var args = [String]()
        
        if force {
            args += ["-f"]
        }

        args += [
            inputFile.prependIfRelativePath(projectDirectory),
            "--output-path",
            outputPath.prependIfRelativePath(projectDirectory),
            "--langs",
            langs,
            "--default-lang",
            defaultLang,
            "--extension-output-path",
            extensionOutputPath.prependIfRelativePath(projectDirectory),
            "--static-members",
            "\(staticMembersOptions)",
            "--xc-strings",
            "\(xcStringsOptions)"
        ]
        
        if let extensionName = extensionName {
            args += [
                "--extension-name",
                extensionName
            ]
        }
        
        if let extensionSuffix = extensionSuffix {
            args += [
                "--extension-suffix",
                extensionSuffix
            ]
        }
        
        Stringium.main(args)
    }
}
