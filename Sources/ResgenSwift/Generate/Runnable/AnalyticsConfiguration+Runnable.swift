//
//  AnalyticsConfiguration+Runnable.swift
//  
//
//  Created by Loris Perret on 08/12/2023.
//

import Foundation

extension AnalyticsConfiguration: Runnable {
    func run(projectDirectory: String, force: Bool) {
        var args = [String]()
        
        if force {
            args += ["-f"]
        }
        
        args += [
            inputFile.prependIfRelativePath(projectDirectory),
            "--target",
            target,
            "--extension-output-path",
            extensionOutputPath.prependIfRelativePath(projectDirectory),
            "--static-members",
            "\(staticMembersOptions)"
        ]
        
        if let extensionName = extensionName {
            args += [
                "--extension-name",
                extensionName
            ]
        }
        if let extensionSuffix = extensionSuffix {
            args += [
                "--extension-suffix",
                extensionSuffix
            ]
        }
        
        Analytics.main(args)
    }
}
