//
//  ResgenSwiftError.swift
//  
//
//  Created by Thibaut Schmitt on 30/08/2022.
//

import Foundation

enum GenerateError: Error {
    case fileNotExists(String)
    case invalidConfigurationFile(String, String)
    case commandError([String], String)
    case writeFile(String, String)
    
    var description: String {
        switch self {
        case .fileNotExists(let filename):
            return "error: [\(Generate.toolName)] File \(filename) does not exists"
    
        case .invalidConfigurationFile(let filename, let underneathErrorDescription):
            return "error: [\(Generate.toolName)] File \(filename) is not a valid configuration file. Underneath error: \(underneathErrorDescription)"
            
        case .commandError(let command, let terminationStatus):
            let readableCommand = command
                .map { $0 }
                .joined(separator: " ")
            return "error: [\(Generate.toolName)] An error occured while running command '\(readableCommand)'. Command terminate with status code: \(terminationStatus)"
            
        case .writeFile(let filename, let info):
            return "error: [\(Generate.toolName)] An error occured while writing file in \(filename): \(info)"
        }
    }
}
