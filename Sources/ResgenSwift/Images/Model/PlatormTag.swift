//
//  PlatormTag.swift
//  
//
//  Created by Thibaut Schmitt on 29/08/2022.
//

import Foundation

enum PlatormTag: String {
    case droid = "d"
    case ios = "i"
}
