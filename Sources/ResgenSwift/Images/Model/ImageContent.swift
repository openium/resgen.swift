//
//  ImageContent.swift
//
//
//  Created by Quentin Bandera on 19/04/2024.
//

import Foundation

enum TemplateRenderingIntent: String, Codable {
    case template
    case original
}

struct AssetContent: Codable, Equatable {
    let images: [AssetImageDescription]
    let info: AssetInfo
    let properties: AssetProperties?

    init(
        images: [AssetImageDescription],
        info: AssetInfo,
        properties: AssetProperties? = nil
    ) {
        self.images = images
        self.info = info
        self.properties = properties
    }

    static func == (lhs: AssetContent, rhs: AssetContent) -> Bool {
        guard lhs.images.count == rhs.images.count else { return false }
        let lhsImages = lhs.images.sorted(by: { $0.filename < $1.filename })
        let rhsImages = rhs.images.sorted(by: { $0.filename < $1.filename })

        return lhsImages == rhsImages
    }
}

struct AssetImageDescription: Codable, Equatable {
    let idiom: String
    let scale: String?
    let filename: String

    init(
        idiom: String,
        scale: String? = nil,
        filename: String
    ) {
        self.idiom = idiom
        self.scale = scale
        self.filename = filename
    }
}

struct AssetInfo: Codable, Equatable {
    let version: Int
    let author: String
}

struct AssetProperties: Codable, Equatable {
    let preservesVectorRepresentation: Bool
    let templateRenderingIntent: TemplateRenderingIntent?

    init(
        preservesVectorRepresentation: Bool,
        templateRenderingIntent: TemplateRenderingIntent? = nil
    ) {
        self.preservesVectorRepresentation = preservesVectorRepresentation
        self.templateRenderingIntent = templateRenderingIntent
    }

    enum CodingKeys: String, CodingKey {
        case preservesVectorRepresentation = "preserves-vector-representation"
        case templateRenderingIntent = "template-rendering-intent"
    }
}
