//
//  ConvertArgument.swift
//  
//
//  Created by Thibaut Schmitt on 24/01/2022.
//

import Foundation

struct ConvertArgument {
    let width: String?
    let height: String?
}
