//
//  ImagesError.swift
//  
//
//  Created by Thibaut Schmitt on 24/01/2022.
//

import Foundation

enum ImagesError: Error {
    case extensionNamesCollision(String)
    case inputFolderNotFound(String)
    case fileNotExists(String)
    case unknownImageExtension(String)
    case getFileAttributed(String, String)
    case rsvgConvertNotFound
    case writeFile(String, String)
    case createAssetFolder(String)
    case unknown(String)
    
    var description: String {
        switch self {
        case .extensionNamesCollision(let extensionName):
            return "error: [\(Fonts.toolName)] Error on extension names, extension name and SwiftUI extension name should be different (\(extensionName) is used on both)"
            
        case .inputFolderNotFound(let inputFolder):
            return "error: [\(Images.toolName)] Input folder not found: \(inputFolder)"
            
        case .fileNotExists(let filename):
            return "error: [\(Images.toolName)] File \(filename) does not exists"
            
        case .unknownImageExtension(let filename):
            return "error: [\(Images.toolName)] File \(filename) have an unhandled file extension. Cannot generate image."
            
        case .getFileAttributed(let filename, let errorDescription):
            return "error: [\(Images.toolName)] Getting file attributes of \(filename) failed with error: \(errorDescription)"
            
        case .rsvgConvertNotFound:
            return "error: [\(Images.toolName)] Can't find rsvg-convert (can be installed with 'brew remove imagemagick && brew install imagemagick --with-librsvg')"
            
        case .writeFile(let subErrorDescription, let filename):
            return "error: [\(Images.toolName)] An error occured while writing content to \(filename): \(subErrorDescription)"

        case .createAssetFolder(let folder):
            return "error: [\(Colors.toolName)] An error occured while creating folder `\(folder)`"
            
        case .unknown(let errorDescription):
            return "error: [\(Images.toolName)] Unknown error: \(errorDescription)"
        }
    }
}
