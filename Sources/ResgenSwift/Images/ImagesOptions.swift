//
//  ImagiumOptions.swift
//  
//
//  Created by Thibaut Schmitt on 24/01/2022.
//

import Foundation
import ArgumentParser

struct ImagesOptions: ParsableArguments {
    @Flag(name: .customShort("f"), help: "Should force script execution")
    var forceExecution = false
    
    @Flag(name: .customShort("F"), help: "Regenerate all images")
    var forceExecutionAndGeneration = false
    
    @Argument(help: "Input files where strings ared defined.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var inputFile: String
    
    @Option(help: "Xcassets path where to generate images.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var xcassetsPath: String
    
    @Option(help: "Path where to generate the extension.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var extensionOutputPath: String
    
    @Option(help: "Tell if it will generate static properties or not")
    var staticMembers: Bool = false
    
    @Option(help: "Extension name. If not specified, it will generate an Image extension.")
    var extensionName: String = Images.defaultExtensionName
    
    @Option(help: "Extension name. If not specified, it will generate an UIImage extension.")
    var extensionNameUIKit: String = Images.defaultExtensionNameUIKit
    
    @Option(help: "Extension suffix. Ex: MyApp, it will generate {extensionName}+Image{extensionSuffix}.swift")
    var extensionSuffix: String?
}

// MARK: - Computed var

extension ImagesOptions {
    
    // MARK: - SwiftUI
    
    var extensionFileName: String {
        if let extensionSuffix = extensionSuffix {
            return "\(extensionName)+\(extensionSuffix).swift"
        }
        return "\(extensionName).swift"
    }
    
    var extensionFilePath: String {
        "\(extensionOutputPath)/\(extensionFileName)"
    }
    
    // MARK: - UIKit
    
    var extensionFileNameUIKit: String {
        if let extensionSuffix = extensionSuffix {
            return "\(extensionNameUIKit)+\(extensionSuffix).swift"
        }
        return "\(extensionNameUIKit).swift"
    }
    
    var extensionFilePathUIKit: String {
        "\(extensionOutputPath)/\(extensionFileNameUIKit)"
    }
    
    // MARK: -
    
    var inputFilenameWithoutExt: String {
        URL(fileURLWithPath: inputFile)
            .deletingPathExtension()
            .lastPathComponent
    }
}
