//
//  ImageFileParser.swift
//  
//
//  Created by Thibaut Schmitt on 24/01/2022.
//

import Foundation

class ImageFileParser {
    
    static func parse(_ inputFile: String, platform: PlatormTag) -> [ParsedImage] {
        let inputFileContent = try! String(contentsOfFile: inputFile, encoding: .utf8)
        let stringsByLines = inputFileContent.components(separatedBy: .newlines)
        
        return Self.parseLines(stringsByLines, platform: platform)
    }
    
    static func parseLines(_ lines: [String], platform: PlatormTag) -> [ParsedImage] {
        var imagesToGenerate = [ParsedImage]()
        
        lines.forEach {
            guard $0.removeLeadingTrailingWhitespace().isEmpty == false, $0.first != "#" else {
                return
            }
            
            let splittedLine = $0.split(separator: " ")
            
            let width: Int = {
                if splittedLine[2] == "?" {
                    return -1
                }
                return Int(splittedLine[2])!
            }()
            let height: Int = {
                if splittedLine[3] == "?" {
                    return -1
                }
                return Int(splittedLine[3])!
            }()

            var imageExtensions: [ImageExtension] = []

            splittedLine.forEach { stringExtension in
                if let imageExtension = ImageExtension(rawValue: String(stringExtension)) {
                    imageExtensions.append(imageExtension)
                }
            }

            let image = ParsedImage(name: String(splittedLine[1]), tags: String(splittedLine[0]), width: width, height: height, imageExtensions: imageExtensions)
            imagesToGenerate.append(image)
        }

        print(imagesToGenerate)

        return imagesToGenerate.filter {
            $0.tags.contains(platform.rawValue)
        }
    }
}
