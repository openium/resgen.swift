//
//  ResgenSwift.swift
//
//
//  Created by Thibaut Schmitt on 13/12/2021.
//

import ToolCore
import Foundation
import ArgumentParser

struct ResgenSwift: ParsableCommand {
    
    static var configuration = CommandConfiguration(
        abstract: "A utility for generate ressources.",
        version: ResgenSwiftVersion,

        // Pass an array to `subcommands` to set up a nested tree of subcommands.
        // With language support for type-level introspection, this could be
        // provided by automatically finding nested `ParsableCommand` types.
        subcommands: [
            Analytics.self,
            Colors.self,
            Fonts.self,
            Images.self,
            Strings.self,
            Tags.self,
            Generate.self
        ]

        // A default subcommand, when provided, is automatically selected if a
        // subcommand is not given on the command line.
        //defaultSubcommand: Twine.self
    )
}

ResgenSwift.main()
