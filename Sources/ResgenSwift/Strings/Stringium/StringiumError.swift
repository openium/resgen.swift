//
//  StringToolError.swift
//  
//
//  Created by Thibaut Schmitt on 05/01/2022.
//

import Foundation

enum StringiumError: Error {
    case fileNotExists(String)
    case langsListEmpty
    case defaultLangsNotInLangs
    case writeFile(String, String)
    case langNotDefined(String, String, Bool)
    
    var description: String {
        switch self {
        case .fileNotExists(let filename):
            return "error: [\(Stringium.toolName)] File \(filename) does not exists "
            
        case .langsListEmpty:
            return "error: [\(Stringium.toolName)] Langs list is empty"
            
        case .defaultLangsNotInLangs:
            return "error: [\(Stringium.toolName)] Langs list does not contains the default lang"
            
        case .writeFile(let subErrorDescription, let filename):
            return "error: [\(Stringium.toolName)] An error occured while writing content to \(filename): \(subErrorDescription)"
            
        case .langNotDefined(let lang, let definitionName, let isReference):
            if isReference {
                return "error: [\(Stringium.toolName)] Reference are handled only by Twine. Please use it or remove reference from you strings file."
            }
            return "error: [\(Stringium.toolName)] Lang \"\(lang)\" not found for \"\(definitionName)\""
        }
    }
}
