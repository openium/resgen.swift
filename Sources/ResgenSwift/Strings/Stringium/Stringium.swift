//
//  Stringium.swift
//  
//
//  Created by Thibaut Schmitt on 10/01/2022.
//

import ToolCore
import Foundation
import ArgumentParser

struct Stringium: ParsableCommand {
    
    // MARK: - Command Configuration
    
    static var configuration = CommandConfiguration(
        abstract: "Generate strings with custom scripts.",
        version: ResgenSwiftVersion
    )
    
    // MARK: - Static
    
    static let toolName = "Stringium"
    static let defaultExtensionName = "String"
    static let noTranslationTag: String = "notranslation"
    
    // MARK: - Command options
    
    @OptionGroup var options: StringiumOptions
    
    // MARK: - Run
    
    mutating func run() {
        print("[\(Self.toolName)] Starting strings generation")
        print("[\(Self.toolName)] Will use inputFile \(options.inputFile) to generate strings for \(options.langs) (default lang: \(options.defaultLang)")
        
        // Check requirements
        guard checkRequirements() else { return }
        
        print("[\(Self.toolName)] Will generate strings")
        
        // Parse input file
        let sections = TwineFileParser.parse(options.inputFile)
        
        // Generate strings files
        print(options.xcStrings)
        if !options.xcStrings {
            print("[\(Self.toolName)] Will generate strings")

            StringsFileGenerator.writeStringsFiles(sections: sections,
                                                   langs: options.langs,
                                                   defaultLang: options.defaultLang,
                                                   tags: options.tags,
                                                   outputPath: options.stringsFileOutputPath,
                                                   inputFilenameWithoutExt: options.inputFilenameWithoutExt)
        } else {
            print("[\(Self.toolName)] Will generate xcStrings")
            StringsFileGenerator.writeXcStringsFiles(sections: sections,
                                                     langs: options.langs,
                                                     defaultLang: options.defaultLang,
                                                     tags: options.tags,
                                                     outputPath: options.stringsFileOutputPath,
                                                     inputFilenameWithoutExt: options.inputFilenameWithoutExt)
        }

        // Generate extension
        StringsFileGenerator.writeExtensionFiles(sections: sections,
                                                 defaultLang: options.defaultLang,
                                                 tags: options.tags,
                                                 staticVar: options.staticMembers,
                                                 inputFilename: options.inputFilenameWithoutExt,
                                                 extensionName: options.extensionName,
                                                 extensionFilePath: options.extensionFilePath,
                                                 extensionSuffix: options.extensionSuffix)

        print("[\(Self.toolName)] Strings generated")
    }
    
    // MARK: - Requirements
    
    private func checkRequirements() -> Bool {
        let fileManager = FileManager()
        
        // Input file
        guard fileManager.fileExists(atPath: options.inputFile) else {
            let error = StringiumError.fileNotExists(options.inputFile)
            print(error.description)
            Stringium.exit(withError: error)
        }
        
        // Langs
        guard options.langs.isEmpty == false else {
            let error = StringiumError.langsListEmpty
            print(error.description)
            Stringium.exit(withError: error)
        }
        
        guard options.langs.contains(options.defaultLang) else {
            let error = StringiumError.defaultLangsNotInLangs
            print(error.description)
            Stringium.exit(withError: error)
        }
        
        // Check if needed to regenerate
        guard GeneratorChecker.shouldGenerate(force: options.forceGeneration,
                                              inputFilePath: options.inputFile,
                                              extensionFilePath: options.extensionFilePath) else {
            print("[\(Self.toolName)] Strings are already up to date :) ")
            return false
        }
        
        return true
    }
}
