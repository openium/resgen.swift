//
//  StringiumOptions.swift
//  
//
//  Created by Thibaut Schmitt on 10/01/2022.
//

import Foundation
import ArgumentParser

struct StringiumOptions: ParsableArguments {
    @Flag(name: [.customShort("f"), .customShort("F")], help: "Should force generation")
    var forceGeneration = false

    @Argument(help: "Input files where strings are defined.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var inputFile: String
    
    @Option(name: .customLong("output-path"), help: "Path where to strings file.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    fileprivate var outputPathRaw: String
    
    @Option(name: .customLong("langs"), help: "Langs to generate.")
    fileprivate var langsRaw: String
    
    @Option(help: "Default langs.")
    var defaultLang: String
    
    @Option(name: .customLong("tags"), help: "Tags to generate.")
    fileprivate var tagsRaw: String = "ios iosonly iosOnly notranslation"
    
    @Option(help: "Path where to generate the extension.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var extensionOutputPath: String
    
    @Option(help: "Tell if it will generate static properties or not")
    var staticMembers: Bool = false

    @Option(help: "Tell if it will generate xcStrings file or not")
    var xcStrings: Bool = false

    @Option(help: "Extension name. If not specified, it will generate an String extension.")
    var extensionName: String = Stringium.defaultExtensionName
    
    @Option(help: "Extension suffix: {extensionName}+{extensionSuffix}.swift")
    var extensionSuffix: String
}

// MARK: - Private var getter

extension StringiumOptions {
    var stringsFileOutputPath: String {
        var outputPath = outputPathRaw
        if outputPath.last == "/" {
            outputPath = String(outputPath.dropLast())
        }
        return outputPath
    }
    
    var langs: [String] {
        langsRaw
            .split(separator: " ")
            .map { String($0) }
    }
    
    var tags: [String] {
        tagsRaw
            .split(separator: " ")
            .map { String($0) }
    }
}

// MARK: - Computed var

extension StringiumOptions {
    var extensionFileName: String {
        "\(extensionName)+\(extensionSuffix).swift"
    }
    
    var extensionFilePath: String {
        "\(extensionOutputPath)/\(extensionFileName)"
    }
    
    var inputFilenameWithoutExt: String {
        URL(fileURLWithPath: inputFile)
            .deletingPathExtension()
            .lastPathComponent
    }
}
