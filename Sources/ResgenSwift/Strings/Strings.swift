//
//  main.swift
//  
//
//  Created by Thibaut Schmitt on 10/01/2022.
//

import ToolCore
import Foundation
import ArgumentParser

struct Strings: ParsableCommand {
    
    static var configuration = CommandConfiguration(
        abstract: "A utility for generate strings.",
        version: ResgenSwiftVersion,

        // Pass an array to `subcommands` to set up a nested tree of subcommands.
        // With language support for type-level introspection, this could be
        // provided by automatically finding nested `ParsableCommand` types.
        subcommands: [Twine.self, Stringium.self, Tags.self]

        // A default subcommand, when provided, is automatically selected if a
        // subcommand is not given on the command line.
        //defaultSubcommand: Twine.self
    )
}

//Strings.main()
