//
//  TagOptions.swift
//  
//
//  Created by Thibaut Schmitt on 10/01/2022.
//

import Foundation
import ArgumentParser

struct TagsOptions: ParsableArguments {
    @Flag(name: [.customShort("f"), .customShort("F")], help: "Should force generation")
    var forceGeneration = false
    
    @Argument(help: "Input files where tags ared defined.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var inputFile: String
    
    @Option(help: "Lang to generate. (\"ium\" by default)")
    var lang: String = "ium"
    
    @Option(help: "Path where to generate the extension.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var extensionOutputPath: String
    
    @Option(help: "Tell if it will generate static properties or not")
    var staticMembers: Bool = false
    
    @Option(help: "Extension name. If not specified, it will generate a Tag extension.")
    var extensionName: String = Tags.defaultExtensionName
    
    @Option(help: "Extension suffix. Ex: MyApp, it will generate {extensionName}+Tag{extensionSuffix}.swift")
    var extensionSuffix: String?
}

// MARK: - Computed var

extension TagsOptions {
    var extensionFileName: String {
        if let extensionSuffix = extensionSuffix {
            return "\(extensionName)+\(extensionSuffix).swift"
        }
        return "\(extensionName).swift"
    }
    
    var extensionFilePath: String {
        "\(extensionOutputPath)/\(extensionFileName)"
    }
}
