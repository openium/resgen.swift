//
//  XcString.swift
//
//
//  Created by Quentin Bandera on 12/04/2024.
//

import SwiftUI

struct DynamicKey: CodingKey {
    var intValue: Int?
    init?(intValue: Int) {
        self.intValue = intValue
        self.stringValue = "\(intValue)"
    }

    var stringValue: String
    init?(stringValue: String) {
        self.stringValue = stringValue
    }
}

struct Root: Codable, Equatable {
    let sourceLanguage: String
    let strings: XCStringDefinitionContainer
    let version: String
}

struct XCStringDefinitionContainer: Codable, Equatable {
    let strings: [XCStringDefinition]

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: DynamicKey.self)

        for str in strings {
            if let codingKey = DynamicKey(stringValue: str.title) {
                try container.encode(str.content, forKey: codingKey)
            }
        }
    }

    static func == (lhs: XCStringDefinitionContainer, rhs: XCStringDefinitionContainer) -> Bool {
        return lhs.strings.sorted(by: {
            $0.title < $1.title
        }) == rhs.strings.sorted(by: { $0.title < $1.title })
    }
}

struct XCStringDefinition: Codable, Equatable {
    let title: String // json key -> custom encoding methods
    let content: XCStringDefinitionContent
}

struct XCStringDefinitionContent: Codable, Equatable {
    let comment: String?
    let extractionState: String
    var localizations: XCStringLocalizationContainer

    init(comment: String? = nil, extractionState: String, localizations: XCStringLocalizationContainer) {
        self.comment = comment
        self.extractionState = extractionState
        self.localizations = localizations
    }
}

struct XCStringLocalizationContainer: Codable, Equatable {
    let localizations: [XCStringLocalization]

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: DynamicKey.self)

        for loca in localizations {
            if let codingKey = DynamicKey(stringValue: loca.lang) {
                try container.encode(loca.content, forKey: codingKey)
            }
        }
    }

    static func == (lhs: XCStringLocalizationContainer, rhs: XCStringLocalizationContainer) -> Bool {
        return lhs.localizations.count == rhs.localizations.count && lhs.localizations.sorted(by: { $0.lang < $1.lang }) == rhs.localizations.sorted(by: { $0.lang < $1.lang })
    }
}

struct XCStringLocalization: Codable, Equatable {
    let lang: String // json key -> custom encoding method
    let content: XCStringLocalizationLangContent
}

struct XCStringLocalizationLangContent: Codable, Equatable {
    let stringUnit: DefaultStringUnit
}

//enum VarationOrStringUnit: Encodable {
//    case variations([Varation])
//    case stringUnit: (DefaultStringUnit)
//
//    func encode(to encoder: any Encoder) throws {
//        if let varations {
//
//        } else if let  {
//
//        }
//    }
//}

struct DefaultStringUnit: Codable, Equatable {
    let state: String
    let value: String
}
