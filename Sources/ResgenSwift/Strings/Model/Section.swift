//
//  Section.swift
//  
//
//  Created by Thibaut Schmitt on 04/01/2022.
//

import Foundation

class Section {
    let name: String // OnBoarding
    var definitions = [Definition]()
    
    init(name: String) {
        self.name = name
    }
    
    static func match(_ line: String) -> Section? {
        guard line.range(of: "\\[\\[(.*?)]]$", options: .regularExpression, range: nil, locale: nil) != nil else {
            return nil
        }
        
        let sectionName = line
            .replacingOccurrences(of: ["[", "]"], with: "")
            .removeLeadingTrailingWhitespace()
        return Section(name: sectionName)
    }
    
    func hasOneOrMoreMatchingTags(tags: [String]) -> Bool {
        let allTags = definitions.flatMap { $0.tags }
        let allTagsSet = Set(allTags)
        
        let intersection = Set(tags).intersection(allTagsSet)
        if intersection.isEmpty {
            return false
        }
        return true
    }
}
