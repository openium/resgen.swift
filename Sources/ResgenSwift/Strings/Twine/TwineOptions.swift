//
//  TwineOptions.swift
//  
//
//  Created by Thibaut Schmitt on 10/01/2022.
//

import Foundation
import ArgumentParser

struct TwineOptions: ParsableArguments {
    @Flag(name: [.customShort("f"), .customShort("F")], help: "Should force generation")
    var forceGeneration = false
    
    @Argument(help: "Input files where strings ared defined.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var inputFile: String
    
    @Option(help: "Path where to strings file.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var outputPath: String
    
    @Option(name: .customLong("langs"), help: "Langs to generate.")
    fileprivate var langsRaw: String
    
    @Option(help: "Default langs.")
    var defaultLang: String
    
    @Option(help: "Path where to generate the extension.", transform: { $0.replaceTiltWithHomeDirectoryPath() })
    var extensionOutputPath: String
}

// MARK: - Private var getter

extension TwineOptions {
    var langs: [String] {
        langsRaw
            .split(separator: " ")
            .map { String($0) }
    }
}

// MARK: - Computed var

extension TwineOptions {
    var inputFilenameWithoutExt: String {
        URL(fileURLWithPath: inputFile)
            .deletingPathExtension()
            .lastPathComponent
    }
    
    var extensionFilePath: String {
        "\(extensionOutputPath)/\(inputFilenameWithoutExt).swift"
    }
    
    // "R2String+" is hardcoded in Twine formatter
    var extensionFilePathGenerated: String {
        "\(extensionOutputPath)/R2String+\(inputFilenameWithoutExt).swift"
    }
}
