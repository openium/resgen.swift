//
//  TwineError.swift
//  
//
//  Created by Thibaut Schmitt on 10/01/2022.
//

import Foundation

enum TwineError: Error {
    case fileNotExists(String)
    case langsListEmpty
    case defaultLangsNotInLangs
    
    var description: String {
        switch self {
        case .fileNotExists(let filename):
            return "error: [\(Twine.toolName)] File \(filename) does not exists "
            
        case .langsListEmpty:
            return "error: [\(Twine.toolName)] Langs list is empty"
            
        case .defaultLangsNotInLangs:
            return "error: [\(Twine.toolName)] Langs list does not contains the default lang"
        }
    }
}
