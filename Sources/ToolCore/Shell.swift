//
//  Shell.swift
//  
//
//  Created by Thibaut Schmitt on 22/12/2021.
//

import Foundation

public class Shell {
    
    public static var environment: [String: String] {
        ProcessInfo.processInfo.environment
    }
    
//    @discardableResult
//    public static func shell(launchPath: String = "/usr/bin/env", _ args: String...) -> (terminationStatus: Int32, output: String?) {
//        let task = Process()
//        task.launchPath = launchPath
//        task.arguments = args
//
//        var currentEnv = ProcessInfo.processInfo.environment
//        for (key, value) in environment {
//            currentEnv[key] = value
//        }
//        task.environment = currentEnv
//
//        let pipe = Pipe()
//        task.standardOutput = pipe
//        try? task.run()
//        task.waitUntilExit()
//
//        let data = pipe.fileHandleForReading.readDataToEndOfFile()
//
//        guard let output: String = String(data: data, encoding: .utf8) else {
//            return (terminationStatus: task.terminationStatus, output: nil)
//        }
//
//        return (terminationStatus: task.terminationStatus, output: output)
//    }
    
    @discardableResult
    public static func shell(launchPath: String = "/usr/bin/env", _ args: [String]) -> (terminationStatus: Int32, output: String?) {
        let task = Process()
        task.launchPath = launchPath
        task.arguments = args
        
        var currentEnv = ProcessInfo.processInfo.environment
        for (key, value) in environment {
            currentEnv[key] = value
        }
        task.environment = currentEnv
        
        let pipe = Pipe()
        task.standardOutput = pipe
        task.launch()
        task.waitUntilExit()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        
        guard let output: String = String(data: data, encoding: .utf8) else {
            return (terminationStatus: task.terminationStatus, output: nil)
        }
        
        return (terminationStatus: task.terminationStatus, output: output)
    }
}
