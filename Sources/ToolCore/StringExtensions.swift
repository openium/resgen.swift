//
//  Extensions.swift
//  
//
//  Created by Thibaut Schmitt on 13/12/2021.
//

import Foundation

public extension String {
    func removeCharacters(from forbiddenChars: CharacterSet) -> String {
        let passed = self.unicodeScalars.filter { !forbiddenChars.contains($0) }
        return String(String.UnicodeScalarView(passed))
    }
    
    func removeCharacters(from: String) -> String {
        return removeCharacters(from: CharacterSet(charactersIn: from))
    }
    
    func replacingOccurrences(of: [String], with: String) -> Self {
        var tmp = self
        for e in of {
            tmp = tmp.replacingOccurrences(of: e, with: with)
        }
        return tmp
    }
    
    func removeTrailingWhitespace() -> Self {
        var newString = self
        
        while newString.last?.isWhitespace == true {
            newString = String(newString.dropLast())
        }
        
        return newString
    }
    
    func removeLeadingWhitespace() -> Self {
        var newString = self
        
        while newString.first?.isWhitespace == true {
            newString = String(newString.dropFirst())
        }
        
        return newString
    }
    
    func removeLeadingTrailingWhitespace() -> Self {
        var newString = self
        
        newString = newString.removeLeadingWhitespace()
        newString = newString.removeTrailingWhitespace()
        
        return newString
    }
    
    func escapeDoubleQuote() -> Self {
        replacingOccurrences(of: "\"", with: "\\\"")
    }
    
    func replaceTiltWithHomeDirectoryPath() -> Self {
        // See NSString.expandingTildeInPath
        replacingOccurrences(of: "~", with: "\(FileManager.default.homeDirectoryForCurrentUser.relativePath)")
    }
    
    func colorComponent() -> (alpha: String, red: String, green: String, blue: String) {
        var alpha: String = "FF"
        var red: String
        var green: String
        var blue: String

        var colorClean = self
                .replacingOccurrences(of: "#", with: "")
                .replacingOccurrences(of: "0x", with: "")

        if colorClean.count == 8 {
            alpha = String(colorClean.prefix(2))
            colorClean = String(colorClean.dropFirst(2))
        }
        
        red = String(colorClean.prefix(2))
        colorClean = String(colorClean.dropFirst(2))
        green = String(colorClean.prefix(2))
        colorClean = String(colorClean.dropFirst(2))
        blue = String(colorClean.prefix(2))
        return (alpha: alpha, red: red, green: green, blue: blue)
    }
    
    func uppercasedFirst() -> String {
        prefix(1).uppercased() + dropFirst()
    }

    func replacingFirstOccurrence(of: String, with: String) -> Self {
        if let range = self.range(of: of) {
            let tmp = self.replacingOccurrences(
                of: of,
                with: with,
                options: .literal,
                range: range
            )

            return tmp
        }

        return self
    }
}
