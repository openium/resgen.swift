//
//  Version.swift
//  
//
//  Created by Thibaut Schmitt on 25/07/2022.
//

import Foundation

public let ResgenSwiftVersion = "1.2"
