//
//  ImageFileParserTests.swift
//  
//
//  Created by Thibaut Schmitt on 05/09/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

class ImageFileParserTests: XCTestCase {
    func testParseImagesFile() {
        // Given
        let input = """
        #
        # SMAAS Support
        #
        id image_one 25 ? png
        di image_two ? 50 webp png
        d image_three 25 ?
        d image_four 75 ?
        """
            .components(separatedBy: CharacterSet.newlines)
        
        // When
        let parsedImages = ImageFileParser.parseLines(input,
                                                      platform: PlatormTag.ios)
                
        // Expect
        XCTAssertEqual(parsedImages.count, 2)
        
        let firstImage = parsedImages.first {
            $0.name == "image_one"
        }
        XCTAssertEqual(firstImage!.name, "image_one")
        XCTAssertEqual(firstImage!.tags, "id")
        XCTAssertEqual(firstImage!.width, 25)
        XCTAssertEqual(firstImage!.height, -1)
        XCTAssertEqual(firstImage!.imageExtensions, [.png])

        let secondImage = parsedImages.first {
            $0.name == "image_two"
        }
        XCTAssertEqual(secondImage!.name, "image_two")
        XCTAssertEqual(secondImage!.tags, "di")
        XCTAssertEqual(secondImage!.width, -1)
        XCTAssertEqual(secondImage!.height, 50)     
        XCTAssertEqual(firstImage!.imageExtensions, [.png])
    }
}
