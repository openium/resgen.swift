//
//  ParsedImage.swift
//  
//
//  Created by Thibaut Schmitt on 05/09/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class ParsedImageTests: XCTestCase {
    
    func testConvertArguments() {
        // Given
        let imageName = "the_name"
        let parsedImage = ParsedImage(name: imageName,
                                      tags: "id",
                                      width: 10,
                                      height: 10)
        
        // When
        let convertArguments = parsedImage.convertArguments
        
        // Expect
        XCTAssertEqual(convertArguments.x1.width, "10")
        XCTAssertEqual(convertArguments.x1.height, "10")
        
        XCTAssertEqual(convertArguments.x2.width, "20")
        XCTAssertEqual(convertArguments.x2.height, "20")
        
        XCTAssertEqual(convertArguments.x3.width, "30")
        XCTAssertEqual(convertArguments.x3.height, "30")
    }
    
    func test_uiKit_GeneratedProperty() {
        // Given
        let imageName = "the_name"
        let parsedImage = ParsedImage(name: imageName,
                                      tags: "id",
                                      width: 10,
                                      height: 10)
        
        // When
        let property = parsedImage.getImageProperty(isStatic: false, isSwiftUI: false)
        
        // Expect
        let expect = """
        var \(imageName): UIImage {
            UIImage(named: "\(imageName)")!
        }
        """
        
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_uiKit_GeneratedStaticProperty() {
        // Given
        let imageName = "the_name"
        let parsedImage = ParsedImage(name: imageName,
                                      tags: "id",
                                      width: 10,
                                      height: 10)
        
        // When
        let property = parsedImage.getImageProperty(isStatic: true, isSwiftUI: false)
        
        // Expect
        let expect = """
        static var \(imageName): UIImage {
            UIImage(named: "\(imageName)")!
        }
        """
        
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_swiftUI_GeneratedProperty() {
        // Given
        let imageName = "the_name"
        let parsedImage = ParsedImage(name: imageName,
                                      tags: "id",
                                      width: 10,
                                      height: 10)
        
        // When
        let property = parsedImage.getImageProperty(isStatic: false, isSwiftUI: true)
        
        // Expect
        let expect = """
        var \(imageName): Image {
            Image("\(imageName)")
        }
        """
        
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_swiftUI_GeneratedStaticProperty() {
        // Given
        let imageName = "the_name"
        let parsedImage = ParsedImage(name: imageName,
                                      tags: "id",
                                      width: 10,
                                      height: 10)
        
        // When
        let property = parsedImage.getImageProperty(isStatic: true, isSwiftUI: true)
        
        // Expect
        let expect = """
        static var \(imageName): Image {
            Image("\(imageName)")
        }
        """
        
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func testAssetContentJson() {
        // Given
        let imageName = "the_name"
        let parsedImage = ParsedImage(name: imageName,
                                      tags: "id",
                                      width: 10,
                                      height: 10)
        
        // When
        let property = parsedImage.generateImageContent(isVector: false)

        // Expect
        let expect = AssetContent(
            images: [
                AssetImageDescription(
                    idiom: "universal",
                    scale: "1x",
                    filename: "\(parsedImage.name).\(OutputImageExtension.png.rawValue)"
                ),
                AssetImageDescription(
                    idiom: "universal",
                    scale: "2x",
                    filename: "\(parsedImage.name)@2x.\(OutputImageExtension.png.rawValue)"
                ),
                AssetImageDescription(
                    idiom: "universal",
                    scale: "3x",
                    filename: "\(parsedImage.name)@3x.\(OutputImageExtension.png.rawValue)"
                )
            ],
            info: AssetInfo(
                version: 1,
                author: "ResgenSwift-Imagium"
            )
        )

        XCTAssertEqual(property, expect)
    }

    func testAssetPng() {
        // Given
        let imageName = "the_name"
        let parsedImage = ParsedImage(name: imageName,
                                      tags: "id",
                                      width: 10,
                                      height: 10,
                                      imageExtensions: [.png])

        // When
        let property = parsedImage.generateImageContent(isVector: false)

        // Expect

        let expect = AssetContent(
            images: [
                AssetImageDescription(
                    idiom: "universal",
                    scale: "1x",
                    filename: "\(parsedImage.name).\(OutputImageExtension.png.rawValue)"
                ),
                AssetImageDescription(
                    idiom: "universal",
                    scale: "2x",
                    filename: "\(parsedImage.name)@2x.\(OutputImageExtension.png.rawValue)"
                ),
                AssetImageDescription(
                    idiom: "universal",
                    scale: "3x",
                    filename: "\(parsedImage.name)@3x.\(OutputImageExtension.png.rawValue)"
                )
            ],
            info: AssetInfo(
                version: 1,
                author: "ResgenSwift-Imagium"
            )
        )

        debugPrint(property)
        debugPrint(expect)

        XCTAssertEqual(property, expect)
    }
}
