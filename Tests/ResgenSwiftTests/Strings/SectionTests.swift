//
//  SectionTests.swift
//  
//
//  Created by Thibaut Schmitt on 06/09/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class SectionTests: XCTestCase {
    
    // MARK: - Match line
    
    func testMatchingDefinition() {
        // Given
        let line = "[[section_name]]"
        
        // When
        let section = Section.match(line)
        
        // Expect
        XCTAssertNotNil(section)
        XCTAssertEqual(section?.name, "section_name")
    }
    
    func testNotMatchingDefinition() {
        // Given
        let lines = ["section_name",
                     "[section_name]",
                     "[section_name",
                     "[[section_name",
                     "[[section_name]",
                     "section_name]",
                     "section_name]]",
                     "[section_name]]"]
        
        // When
        let matches = lines.compactMap { Section.match($0) }
        
        // Expect
        XCTAssertEqual(matches.isEmpty, true)
    }
    
    // MARK: - Matching tags
    
    func testMatchingTags() {
        // Given
        let section = Section(name: "section_name")
        section.definitions = [
            {
                let def = Definition(name: "definition_name")
                def.tags = ["ios","iosonly"]
                return def
            }(),
            {
                let def = Definition(name: "definition_name_two")
                def.tags = ["droid","droidonly"]
                return def
            }()
        ]
        
        // When
        let match1 = section.hasOneOrMoreMatchingTags(tags: ["ios"])
        let match2 = section.hasOneOrMoreMatchingTags(tags: ["iosonly"])
        let match3 = section.hasOneOrMoreMatchingTags(tags: ["droid"])
        let match4 = section.hasOneOrMoreMatchingTags(tags: ["droidonly"])
        
        // Expect
        XCTAssertTrue(match1)
        XCTAssertTrue(match2)
        XCTAssertTrue(match3)
        XCTAssertTrue(match4)
    }
    
    func testNotMatchingTags() {
        // Given
        let section = Section(name: "section_name")
        section.definitions = [
            {
                let def = Definition(name: "definition_name")
                def.tags = ["ios","iosonly"]
                return def
            }(),
            {
                let def = Definition(name: "definition_name_two")
                def.tags = ["droid","droidonly"]
                return def
            }()
        ]
        
        // When
        let match1 = section.hasOneOrMoreMatchingTags(tags: ["web"])
        let match2 = section.hasOneOrMoreMatchingTags(tags: ["webonly"])
        let match3 = section.hasOneOrMoreMatchingTags(tags: ["azerty"])
        
        // Expect
        XCTAssertFalse(match1)
        XCTAssertFalse(match2)
        XCTAssertFalse(match3)
    }
}
