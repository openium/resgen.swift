//
//  DefinitionTests.swift
//  
//
//  Created by Thibaut Schmitt on 06/09/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class DefinitionTests: XCTestCase {
 
    // MARK: - Match line
    
    func testMatchingDefinition() {
        // Given
        let line = "[definition_name]"
        
        // When
        let definition = Definition.match(line)
        
        // Expect
        XCTAssertNotNil(definition)
        XCTAssertEqual(definition?.name, "definition_name")
    }
    
    func testNotMatchingDefinition() {
        // Given
        let line1 = "definition_name"
        let line2 = "[definition_name"
        let line3 = "definition_name]"
        
        // When
        let definition1 = Definition.match(line1)
        let definition2 = Definition.match(line2)
        let definition3 = Definition.match(line3)
        
        // Expect
        XCTAssertNil(definition1)
        XCTAssertNil(definition2)
        XCTAssertNil(definition3)
    }
    
    // MARK: - Matching tags
    
    func testMatchingTags() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        
        // When
        let match1 = definition.hasOneOrMoreMatchingTags(inputTags: ["ios"])
        let match2 = definition.hasOneOrMoreMatchingTags(inputTags: ["iosonly"])
        let match3 = definition.hasOneOrMoreMatchingTags(inputTags: ["notranslation"])
        
        
        // Expect
        XCTAssertTrue(match1)
        XCTAssertTrue(match2)
        XCTAssertTrue(match3)
    }
    
    func testNotMatchingTags() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        
        // When
        let match1 = definition.hasOneOrMoreMatchingTags(inputTags: ["droid"])
        let match2 = definition.hasOneOrMoreMatchingTags(inputTags: ["droidonly"])
        let match3 = definition.hasOneOrMoreMatchingTags(inputTags: ["azerty"])
        
        // Expect
        XCTAssertFalse(match1)
        XCTAssertFalse(match2)
        XCTAssertFalse(match3)
    }
    
    // MARK: - getNSLocalizedStringProperty
    
    func testGeneratedNSLocalizedStringProperty() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = "This is a comment"
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]
        
        // When
        let propertyFr = definition.getNSLocalizedStringProperty(forLang: "fr")
        let propertyEn = definition.getNSLocalizedStringProperty(forLang: "en")
        let propertyEnUs = definition.getNSLocalizedStringProperty(forLang: "en-us")
                
        // Expect
        let expectFr = """
        /// Translation in fr :
        /// C'est la traduction francaise
        ///
        /// Comment :
        /// This is a comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "C'est la traduction francaise", comment: "This is a comment")
        }
        """
        
        let expectEn = """
        /// Translation in en :
        /// This is the english translation
        ///
        /// Comment :
        /// This is a comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english translation", comment: "This is a comment")
        }
        """
        
        let expectEnUs = """
        /// Translation in en-us :
        /// This is the english us translation
        ///
        /// Comment :
        /// This is a comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english us translation", comment: "This is a comment")
        }
        """
        
        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedNSLocalizedStringPropertyWithEmptyComment() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = ""
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]

        // When
        let propertyFr = definition.getNSLocalizedStringProperty(forLang: "fr")
        let propertyEn = definition.getNSLocalizedStringProperty(forLang: "en")
        let propertyEnUs = definition.getNSLocalizedStringProperty(forLang: "en-us")

        // Expect
        let expectFr = """
        /// Translation in fr :
        /// C'est la traduction francaise
        ///
        /// Comment :
        /// No comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "C'est la traduction francaise", comment: "")
        }
        """

        let expectEn = """
        /// Translation in en :
        /// This is the english translation
        ///
        /// Comment :
        /// No comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english translation", comment: "")
        }
        """

        let expectEnUs = """
        /// Translation in en-us :
        /// This is the english us translation
        ///
        /// Comment :
        /// No comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english us translation", comment: "")
        }
        """

        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedNSLocalizedStringPropertyWithNoComment() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]

        // When
        let propertyFr = definition.getNSLocalizedStringProperty(forLang: "fr")
        let propertyEn = definition.getNSLocalizedStringProperty(forLang: "en")
        let propertyEnUs = definition.getNSLocalizedStringProperty(forLang: "en-us")

        // Expect
        let expectFr = """
        /// Translation in fr :
        /// C'est la traduction francaise
        ///
        /// Comment :
        /// No comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "C'est la traduction francaise", comment: "")
        }
        """

        let expectEn = """
        /// Translation in en :
        /// This is the english translation
        ///
        /// Comment :
        /// No comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english translation", comment: "")
        }
        """

        let expectEnUs = """
        /// Translation in en-us :
        /// This is the english us translation
        ///
        /// Comment :
        /// No comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english us translation", comment: "")
        }
        """

        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    // MARK: - getNSLocalizedStringStaticProperty

    func testGeneratedNSLocalizedStringStaticProperty() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = "This is a comment"
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]
        
        // When
        let propertyFr = definition.getNSLocalizedStringStaticProperty(forLang: "fr")
        let propertyEn = definition.getNSLocalizedStringStaticProperty(forLang: "en")
        let propertyEnUs = definition.getNSLocalizedStringStaticProperty(forLang: "en-us")
                
        // Expect
        let expectFr = """
        /// Translation in fr :
        /// C'est la traduction francaise
        ///
        /// Comment :
        /// This is a comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "C'est la traduction francaise", comment: "This is a comment")
        }
        """
        
        let expectEn = """
        /// Translation in en :
        /// This is the english translation
        ///
        /// Comment :
        /// This is a comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english translation", comment: "This is a comment")
        }
        """
        
        let expectEnUs = """
        /// Translation in en-us :
        /// This is the english us translation
        ///
        /// Comment :
        /// This is a comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english us translation", comment: "This is a comment")
        }
        """
        
        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedNSLocalizedStringStaticPropertyWithEmptyComment() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = ""
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]

        // When
        let propertyFr = definition.getNSLocalizedStringStaticProperty(forLang: "fr")
        let propertyEn = definition.getNSLocalizedStringStaticProperty(forLang: "en")
        let propertyEnUs = definition.getNSLocalizedStringStaticProperty(forLang: "en-us")

        // Expect
        let expectFr = """
        /// Translation in fr :
        /// C'est la traduction francaise
        ///
        /// Comment :
        /// No comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "C'est la traduction francaise", comment: "")
        }
        """

        let expectEn = """
        /// Translation in en :
        /// This is the english translation
        ///
        /// Comment :
        /// No comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english translation", comment: "")
        }
        """

        let expectEnUs = """
        /// Translation in en-us :
        /// This is the english us translation
        ///
        /// Comment :
        /// No comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english us translation", comment: "")
        }
        """

        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedNSLocalizedStringStaticPropertyWithNoComment() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]

        // When
        let propertyFr = definition.getNSLocalizedStringStaticProperty(forLang: "fr")
        let propertyEn = definition.getNSLocalizedStringStaticProperty(forLang: "en")
        let propertyEnUs = definition.getNSLocalizedStringStaticProperty(forLang: "en-us")

        // Expect
        let expectFr = """
        /// Translation in fr :
        /// C'est la traduction francaise
        ///
        /// Comment :
        /// No comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "C'est la traduction francaise", comment: "")
        }
        """

        let expectEn = """
        /// Translation in en :
        /// This is the english translation
        ///
        /// Comment :
        /// No comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english translation", comment: "")
        }
        """

        let expectEnUs = """
        /// Translation in en-us :
        /// This is the english us translation
        ///
        /// Comment :
        /// No comment
        static var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "This is the english us translation", comment: "")
        }
        """

        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedNSLocalizedStringPropertyWithOneArgument() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = "This is a comment"
        definition.translations = [
            "fr": "Welcome \"%@\" !"
        ]
        
        // When
        let propertyFr = definition.getNSLocalizedStringProperty(forLang: "fr")
        
        // Expect
        let expectFr = """
        /// Translation in fr :
        /// Welcome "%@" !
        ///
        /// Comment :
        /// This is a comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "Welcome \"%@\" !", comment: "This is a comment")
        }
        
        /// Translation in fr :
        /// Welcome "%@" !
        ///
        /// Comment :
        /// This is a comment
        func definition_name(arg0: String) -> String {
            String(format: self.definition_name, arg0)
        }
        """
        
        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
    }
    
    func testGeneratedNSLocalizedStringPropertyWithMultipleArgument() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = "This is a comment"
        definition.translations = [
            "fr": "Welcome \"%@\" ! Your age is %d :) Your weight is %f ;-)"
        ]
        
        // When
        let propertyFr = definition.getNSLocalizedStringProperty(forLang: "fr")
        
        // Expect
        let expectFr = """
        /// Translation in fr :
        /// Welcome "%@" ! Your age is %d :) Your weight is %f ;-)
        ///
        /// Comment :
        /// This is a comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "Welcome \"%@\" ! Your age is %d :) Your weight is %f ;-)", comment: "This is a comment")
        }
        
        /// Translation in fr :
        /// Welcome "%@" ! Your age is %d :) Your weight is %f ;-)
        ///
        /// Comment :
        /// This is a comment
        func definition_name(arg0: String, arg1: Int, arg2: Double) -> String {
            String(format: self.definition_name, arg0, arg1, arg2)
        }
        """
    
        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
    }
    
    func testGeneratedNSLocalizedStringPropertyWithNumberedArguments() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = "This is a comment"
        definition.translations = [
            "fr": "Vous %%: %1$@ %2$@ Age: %3$d",
            "en": "You %%: %2$@ %1$@ Age: %3$d"
        ]
        
        // When
        let propertyFr = definition.getNSLocalizedStringProperty(forLang: "fr")
        let propertyEn = definition.getNSLocalizedStringProperty(forLang: "en")
        
        let expectFr = """
        /// Translation in fr :
        /// Vous %%: %1$@ %2$@ Age: %3$d
        ///
        /// Comment :
        /// This is a comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "Vous %%: %1$@ %2$@ Age: %3$d", comment: "This is a comment")
        }
        
        /// Translation in fr :
        /// Vous %%: %1$@ %2$@ Age: %3$d
        ///
        /// Comment :
        /// This is a comment
        func definition_name(arg0: String, arg1: String, arg2: Int) -> String {
            String(format: self.definition_name, arg0, arg1, arg2)
        }
        """
        
        let expectEn = """
        /// Translation in en :
        /// You %%: %2$@ %1$@ Age: %3$d
        ///
        /// Comment :
        /// This is a comment
        var definition_name: String {
            NSLocalizedString("definition_name", tableName: kStringsFileName, bundle: Bundle.main, value: "You %%: %2$@ %1$@ Age: %3$d", comment: "This is a comment")
        }
        
        /// Translation in en :
        /// You %%: %2$@ %1$@ Age: %3$d
        ///
        /// Comment :
        /// This is a comment
        func definition_name(arg0: String, arg1: String, arg2: Int) -> String {
            String(format: self.definition_name, arg0, arg1, arg2)
        }
        """
    
        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
    }
    
    // MARK: - Raw properties
    
    func testGeneratedRawProperty() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = "This is a comment"
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]
        
        // When
        let propertyFr = definition.getProperty(forLang: "fr")
        let propertyEn = definition.getProperty(forLang: "en")
        let propertyEnUs = definition.getProperty(forLang: "en-us")
        
        // Expect
        let expectFr = """
            /// Translation in fr :
            /// C'est la traduction francaise
            ///
            /// Comment :
            /// This is a comment
            var definition_name: String {
                "C'est la traduction francaise"
            }
        """
        
        let expectEn = """
            /// Translation in en :
            /// This is the english translation
            ///
            /// Comment :
            /// This is a comment
            var definition_name: String {
                "This is the english translation"
            }
        """
        
        let expectEnUs = """
            /// Translation in en-us :
            /// This is the english us translation
            ///
            /// Comment :
            /// This is a comment
            var definition_name: String {
                "This is the english us translation"
            }
        """
        
        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedRawPropertyWithEmptyComment() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = ""
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]

        // When
        let propertyFr = definition.getProperty(forLang: "fr")
        let propertyEn = definition.getProperty(forLang: "en")
        let propertyEnUs = definition.getProperty(forLang: "en-us")

        // Expect
        let expectFr = """
            /// Translation in fr :
            /// C'est la traduction francaise
            ///
            /// Comment :
            /// No comment
            var definition_name: String {
                "C'est la traduction francaise"
            }
        """

        let expectEn = """
            /// Translation in en :
            /// This is the english translation
            ///
            /// Comment :
            /// No comment
            var definition_name: String {
                "This is the english translation"
            }
        """

        let expectEnUs = """
            /// Translation in en-us :
            /// This is the english us translation
            ///
            /// Comment :
            /// No comment
            var definition_name: String {
                "This is the english us translation"
            }
        """

        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedRawPropertyWithNoComment() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]

        // When
        let propertyFr = definition.getProperty(forLang: "fr")
        let propertyEn = definition.getProperty(forLang: "en")
        let propertyEnUs = definition.getProperty(forLang: "en-us")

        // Expect
        let expectFr = """
            /// Translation in fr :
            /// C'est la traduction francaise
            ///
            /// Comment :
            /// No comment
            var definition_name: String {
                "C'est la traduction francaise"
            }
        """

        let expectEn = """
            /// Translation in en :
            /// This is the english translation
            ///
            /// Comment :
            /// No comment
            var definition_name: String {
                "This is the english translation"
            }
        """

        let expectEnUs = """
            /// Translation in en-us :
            /// This is the english us translation
            ///
            /// Comment :
            /// No comment
            var definition_name: String {
                "This is the english us translation"
            }
        """

        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    // MARK: - Raw static properties

    func testGeneratedRawStaticProperty() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = "This is a comment"
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]
        
        // When
        let propertyFr = definition.getStaticProperty(forLang: "fr")
        let propertyEn = definition.getStaticProperty(forLang: "en")
        let propertyEnUs = definition.getStaticProperty(forLang: "en-us")
        
        // Expect
        let expectFr = """
            /// Translation in fr :
            /// C'est la traduction francaise
            ///
            /// Comment :
            /// This is a comment
            static var definition_name: String {
                "C'est la traduction francaise"
            }
        """
        
        let expectEn = """
            /// Translation in en :
            /// This is the english translation
            ///
            /// Comment :
            /// This is a comment
            static var definition_name: String {
                "This is the english translation"
            }
        """
        
        let expectEnUs = """
            /// Translation in en-us :
            /// This is the english us translation
            ///
            /// Comment :
            /// This is a comment
            static var definition_name: String {
                "This is the english us translation"
            }
        """
        
        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedRawStaticPropertyWithEmptyComment() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.comment = ""
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]

        // When
        let propertyFr = definition.getStaticProperty(forLang: "fr")
        let propertyEn = definition.getStaticProperty(forLang: "en")
        let propertyEnUs = definition.getStaticProperty(forLang: "en-us")

        // Expect
        let expectFr = """
            /// Translation in fr :
            /// C'est la traduction francaise
            ///
            /// Comment :
            /// No comment
            static var definition_name: String {
                "C'est la traduction francaise"
            }
        """

        let expectEn = """
            /// Translation in en :
            /// This is the english translation
            ///
            /// Comment :
            /// No comment
            static var definition_name: String {
                "This is the english translation"
            }
        """

        let expectEnUs = """
            /// Translation in en-us :
            /// This is the english us translation
            ///
            /// Comment :
            /// No comment
            static var definition_name: String {
                "This is the english us translation"
            }
        """

        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }

    func testGeneratedRawStaticPropertyWithNoComment() {
        // Given
        let definition = Definition(name: "definition_name")
        definition.tags = ["ios","iosonly","notranslation"]
        definition.translations = [
            "fr": "C'est la traduction francaise",
            "en": "This is the english translation",
            "en-us": "This is the english us translation"
        ]

        // When
        let propertyFr = definition.getStaticProperty(forLang: "fr")
        let propertyEn = definition.getStaticProperty(forLang: "en")
        let propertyEnUs = definition.getStaticProperty(forLang: "en-us")

        // Expect
        let expectFr = """
            /// Translation in fr :
            /// C'est la traduction francaise
            ///
            /// Comment :
            /// No comment
            static var definition_name: String {
                "C'est la traduction francaise"
            }
        """

        let expectEn = """
            /// Translation in en :
            /// This is the english translation
            ///
            /// Comment :
            /// No comment
            static var definition_name: String {
                "This is the english translation"
            }
        """

        let expectEnUs = """
            /// Translation in en-us :
            /// This is the english us translation
            ///
            /// Comment :
            /// No comment
            static var definition_name: String {
                "This is the english us translation"
            }
        """

        XCTAssertEqual(propertyFr.adaptForXCTest(), expectFr.adaptForXCTest())
        XCTAssertEqual(propertyEn.adaptForXCTest(), expectEn.adaptForXCTest())
        XCTAssertEqual(propertyEnUs.adaptForXCTest(), expectEnUs.adaptForXCTest())
    }
}
