//
//  AnalyticsSectionTests.swift
//
//
//  Created by Loris Perret on 06/12/2023.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class AnalyticsSectionTests: XCTestCase {
    
    // MARK: - Matching tags
    
    func testMatchingAnalytics() {
        // Given
        let section = AnalyticsCategory(id: "section_name")
        section.definitions = [
            {
                let def = AnalyticsDefinition(id: "definition_name", name: "", type: .screen)
                def.tags = ["ios","iosonly"]
                return def
            }(),
            {
                let def = AnalyticsDefinition(id: "definition_name_two", name: "", type: .screen)
                def.tags = ["droid","droidonly"]
                return def
            }()
        ]
        
        // When
        let match1 = section.hasOneOrMoreMatchingTags(tags: ["ios"])
        let match2 = section.hasOneOrMoreMatchingTags(tags: ["iosonly"])
        let match3 = section.hasOneOrMoreMatchingTags(tags: ["droid"])
        let match4 = section.hasOneOrMoreMatchingTags(tags: ["droidonly"])
        
        // Expect
        XCTAssertTrue(match1)
        XCTAssertTrue(match2)
        XCTAssertTrue(match3)
        XCTAssertTrue(match4)
    }
    
    func testNotMatchingAnalytics() {
        // Given
        let section = AnalyticsCategory(id: "section_name")
        section.definitions = [
            {
                let def = AnalyticsDefinition(id: "definition_name", name: "", type: .screen)
                def.tags = ["ios","iosonly"]
                return def
            }(),
            {
                let def = AnalyticsDefinition(id: "definition_name_two", name: "", type: .screen)
                def.tags = ["droid","droidonly"]
                return def
            }()
        ]
        
        // When
        let match1 = section.hasOneOrMoreMatchingTags(tags: ["web"])
        let match2 = section.hasOneOrMoreMatchingTags(tags: ["webonly"])
        let match3 = section.hasOneOrMoreMatchingTags(tags: ["azerty"])
        
        // Expect
        XCTAssertFalse(match1)
        XCTAssertFalse(match2)
        XCTAssertFalse(match3)
    }
}
