//
//  AnalyticsDefinitionTests.swift
//
//
//  Created by Loris Perret on 06/12/2023.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class AnalyticsDefinitionTests: XCTestCase {
    
    // MARK: - Matching tags
    
    func testMatchingAnalyticss() {
        // Given
        let definition = AnalyticsDefinition(id: "definition_name", name: "", type: .screen)
        definition.tags = ["ios","iosonly","notranslation"]
        
        // When
        let match1 = definition.hasOneOrMoreMatchingTags(inputTags: ["ios"])
        let match2 = definition.hasOneOrMoreMatchingTags(inputTags: ["iosonly"])
        let match3 = definition.hasOneOrMoreMatchingTags(inputTags: ["notranslation"])
        
        
        // Expect
        XCTAssertTrue(match1)
        XCTAssertTrue(match2)
        XCTAssertTrue(match3)
    }
    
    func testNotMatchingAnalyticss() {
        // Given
        let definition = AnalyticsDefinition(id: "definition_name", name: "", type: .screen)
        definition.tags = ["ios","iosonly","notranslation"]
        
        // When
        let match1 = definition.hasOneOrMoreMatchingTags(inputTags: ["droid"])
        let match2 = definition.hasOneOrMoreMatchingTags(inputTags: ["droidonly"])
        let match3 = definition.hasOneOrMoreMatchingTags(inputTags: ["azerty"])
        
        // Expect
        XCTAssertFalse(match1)
        XCTAssertFalse(match2)
        XCTAssertFalse(match3)
    }
    
    // MARK: - Raw properties
    
    func testGeneratedRawPropertyScreen() {
        // Given
        let definition = AnalyticsDefinition(id: "definition_name", name: "Ecran un", type: .screen)
        definition.path = "ecran_un/"
        
        // When
        let propertyScreen = definition.getProperty()
        
        // Expect
        let expectScreen = """
            func logScreenDefinitionName() {
                logScreen(
                    name: "Ecran un",
                    path: "ecran_un/"
                )
            }
        """
        
        XCTAssertEqual(propertyScreen.adaptForXCTest(), expectScreen.adaptForXCTest())
    }
    
    func testGeneratedRawPropertyEvent() {
        // Given
        let definition = AnalyticsDefinition(id: "definition_name", name: "Ecran un", type: .event)
        
        // When
        let propertyEvent = definition.getProperty()
        
        // Expect
        let expectEvent = """
            func logEventDefinitionName() {
                logEvent(
                    name: "Ecran un",
                    action: "",
                    category: "",
                    params: []
                )
            }
        """
        
        XCTAssertEqual(propertyEvent.adaptForXCTest(), expectEvent.adaptForXCTest())
    }
    
    func testGeneratedRawStaticPropertyScreen() {
        // Given
        let definition = AnalyticsDefinition(id: "definition_name", name: "Ecran un", type: .screen)
        definition.path = "ecran_un/"
        
        // When
        let propertyScreen = definition.getStaticProperty()
        
        // Expect
        let expectScreen = """
            static func logScreenDefinitionName() {
                logScreen(
                    name: "Ecran un",
                    path: "ecran_un/"
                )
            }
        """
        
        XCTAssertEqual(propertyScreen.adaptForXCTest(), expectScreen.adaptForXCTest())
    }
    
    func testGeneratedRawStaticPropertyEvent() {
        // Given
        let definition = AnalyticsDefinition(id: "definition_name", name: "Ecran un", type: .event)
        
        // When
        let propertyEvent = definition.getStaticProperty()
        
        // Expect
        let expectEvent = """
            static func logEventDefinitionName() {
                logEvent(
                    name: "Ecran un",
                    action: "",
                    category: "",
                    params: []
                )
            }
        """
        
        XCTAssertEqual(propertyEvent.adaptForXCTest(), expectEvent.adaptForXCTest())
    }
}
