//
//  StringExtensions.swift
//  
//
//  Created by Thibaut Schmitt on 05/09/2022.
//

import Foundation

extension String {
    
    /// Remove all new lines and leading/trailing whitespaces
    func adaptForXCTest() -> Self {
        self
            .split(separator: "\n")
            .compactMap { String($0).removeLeadingTrailingWhitespace() }
            .joined(separator: " - ")
    }
}
