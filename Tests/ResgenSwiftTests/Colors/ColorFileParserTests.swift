//
//  ColorFileParserTests.swift
//  
//
//  Created by Thibaut Schmitt on 05/09/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class ColorFileParserTests: XCTestCase {
    func testCorrectFormat_lightStyle() throws {
        // Given
        let inputWithEqualSeparator = """
        red1 = #FF0000
        red2 = #FFFF0000
        
        red3 = #FF0000
        red4 = #FFFF0000
        
        red5 = #FF0000 #0000FF
        red6 = #FFFF0000 #FF0000FF
        """
            .components(separatedBy: CharacterSet.newlines)
        
        let inputWithSpaceSeparator = """
        red1 #FF0000
        red2 #FFFF0000
        
        red3 #FF0000
        red4 #FFFF0000
        
        red5 #FF0000 #0000FF
        red6 #FFFF0000 #FF0000FF
        """
            .components(separatedBy: CharacterSet.newlines)
        
        // When
        let colorsFromEqual = ColorFileParser.parseLines(lines: inputWithEqualSeparator,
                                                         colorStyle: .light)
        let colorsFromSpace = ColorFileParser.parseLines(lines: inputWithSpaceSeparator,
                                                         colorStyle: .light)
        
        // Expect
        let colorsValues: [(name: String, light: String, dark: String)] = [
            (name: "red1", light: "#FF0000", dark: "#FF0000"),
            (name: "red2", light: "#FFFF0000", dark: "#FFFF0000"),
            (name: "red3", light: "#FF0000", dark: "#FF0000"),
            (name: "red4", light: "#FFFF0000", dark: "#FFFF0000"),
            (name: "red5", light: "#FF0000", dark: "#FF0000"),
            (name: "red6", light: "#FFFF0000", dark: "#FFFF0000"),
        ]
        
        
        var foundColors = 0
        let allParsedColors = colorsFromEqual + colorsFromSpace
        for parsedColor in allParsedColors {
            let testValues = colorsValues.first { $0.name == parsedColor.name }
            if let testValues = testValues {
                foundColors += 1
                XCTAssertEqual(parsedColor.name, testValues.name)
                XCTAssertEqual(parsedColor.light, testValues.light)
                XCTAssertEqual(parsedColor.dark, testValues.dark)
            }
        }
        
        XCTAssertEqual(foundColors, 12)
    }
    
    func testCorrectFormat_allColorStyle() throws {
        // Given
        let input = """
        lightOnly #FF0000
        lightDark #FF0000 #0000FF
        """
            .components(separatedBy: CharacterSet.newlines)
        
        // When
        let parsedColors = ColorFileParser.parseLines(lines: input,
                                                      colorStyle: .all)
        
        // Expect
        let colorRed1 = parsedColors.first { $0.name == "lightOnly" }
        let colorRed2 = parsedColors.first { $0.name == "lightDark" }
        
        XCTAssertNotNil(colorRed1)
        XCTAssertEqual(colorRed1?.name, "lightOnly")
        XCTAssertEqual(colorRed1?.light, "#FF0000")
        XCTAssertEqual(colorRed1?.dark, "#FF0000")
        
        XCTAssertNotNil(colorRed2)
        XCTAssertEqual(colorRed2?.name, "lightDark")
        XCTAssertEqual(colorRed2?.light, "#FF0000")
        XCTAssertEqual(colorRed2?.dark, "#0000FF")
    }
}
