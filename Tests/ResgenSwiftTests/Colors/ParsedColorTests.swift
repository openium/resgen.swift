//
//  ParsedColorTests.swift
//  
//
//  Created by Thibaut Schmitt on 05/09/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class ParsedColorTests: XCTestCase {

    func test_uiKit_GeneratedProperty() {
        // Given
        let color = ParsedColor(name: "red", light: "#FF0000", dark: "#0000FF")
        
        // When
        let property = color.getColorProperty(isStatic: false, isSwiftUI: false)
        
        // Expect
        let expect = """
        /// Color red is #FF0000 (light) or #0000FF (dark)"
        @objc var red: UIColor {
            UIColor(named: "red")!
        }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_uiKit_GeneratedStaticProperty() {
        // Given
        let color = ParsedColor(name: "red", light: "#FF0000", dark: "#0000FF")
        
        // When
        let property = color.getColorProperty(isStatic: true, isSwiftUI: false)
        
        // Expect
        let expect = """
            /// Color red is #FF0000 (light) or #0000FF (dark)"
            static var red: UIColor {
                UIColor(named: "red")!
            }
        """
        
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_swiftUI_GeneratedProperty() {
        // Given
        let color = ParsedColor(name: "red", light: "#FF0000", dark: "#0000FF")
        
        // When
        let property = color.getColorProperty(isStatic: false, isSwiftUI: true)
        
        // Expect
        let expect = """
        /// Color red is #FF0000 (light) or #0000FF (dark)"
        var red: Color {
            Color("red")
        }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_swiftUI_GeneratedStaticProperty() {
        // Given
        let color = ParsedColor(name: "red", light: "#FF0000", dark: "#0000FF")
        
        // When
        let property = color.getColorProperty(isStatic: true, isSwiftUI: true)
        
        // Expect
        let expect = """
            /// Color red is #FF0000 (light) or #0000FF (dark)"
            static var red: Color {
                Color("red")
            }
        """
        
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func testGeneratedColorAsset() {
        // Given
        let color = ParsedColor(name: "red", light: "#FF0000", dark: "#0000FF")
        
        // When
        let contentJson = color.contentsJSON()
        guard let data = contentJson.data(using: .utf8),
              let parsedJson = try? JSONSerialization.jsonObject(with: data) as? [String: Any] else {
            XCTFail("Cannot convert `contentJSON` string to Data")
            return
        }
        
        let colors = parsedJson["colors"] as! [Any]
        
        for color in colors {
            guard let color = color  as? [String: Any] else {
                XCTFail("Cannot convert color object to Dictonnary")
                return
            }
            if let appearance = color["appearances"] as? [Any] {
                // Appearance is define only for dark appearance
                let firstAppearance = appearance.first! as! [String: Any]
                XCTAssertEqual(firstAppearance["value"] as! String, "dark")
                
                let subColor = color["color"] as! [String: Any]
                let components = subColor["components"] as! [String: Any]
                XCTAssertEqual(components["alpha"] as! String, "0xFF")
                XCTAssertEqual(components["blue"] as! String, "0xFF")
                XCTAssertEqual(components["green"] as! String, "0x00")
                XCTAssertEqual(components["red"] as! String, "0x00")
            } else {
                let subColor = color["color"] as! [String: Any]
                let components = subColor["components"] as! [String: Any]
                XCTAssertEqual(components["alpha"] as! String, "0xFF")
                XCTAssertEqual(components["blue"] as! String, "0x00")
                XCTAssertEqual(components["green"] as! String, "0x00")
                XCTAssertEqual(components["red"] as! String, "0xFF")
            }
        }
    }
}
