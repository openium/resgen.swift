//
//  ImagesConfigurationTests.swift
//  
//
//  Created by Thibaut Schmitt on 04/11/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class ImagesConfigurationTests: XCTestCase {
    
    let projectDirectory = "projectDirectory/"
    
    func test_argsGeneration_requiredArgs() {
        // Given
        let testingConfiguration = ImagesConfiguration(inputFile: "path/to/images.txt",
                                                       xcassetsPath: "path/to/assets.xcassets",
                                                       extensionOutputPath: "Images/Generated",
                                                       extensionName: nil,
                                                       extensionNameUIKit: nil,
                                                       extensionSuffix: nil,
                                                       staticMembers: nil)
        
        // When
        let arguments = testingConfiguration.getArguments(projectDirectory: projectDirectory, force: false)
                
        // Expect
        let expectedArguments = [
            "projectDirectory/path/to/images.txt",
            "--xcassets-path",
            "projectDirectory/path/to/assets.xcassets",
            "--extension-output-path",
            "projectDirectory/Images/Generated",
            "--static-members",
            "false"
        ]
        
        XCTAssertEqual(arguments, expectedArguments)
    }
    
    func test_argsGeneration_allArguments() {
        // Given
        let testingConfiguration = ImagesConfiguration(inputFile: "path/to/images.txt",
                                                       xcassetsPath: "path/to/assets.xcassets",
                                                       extensionOutputPath: "Images/Generated",
                                                       extensionName: "AppUIImage",
                                                       extensionNameUIKit: "AppImage",
                                                       extensionSuffix: "Testing",
                                                       staticMembers: true)
        
        // When
        let arguments = testingConfiguration.getArguments(projectDirectory: projectDirectory, force: true)

        // Expect
        let expectedArguments = [
            "-f",
            "projectDirectory/path/to/images.txt",
            "--xcassets-path",
            "projectDirectory/path/to/assets.xcassets",
            "--extension-output-path",
            "projectDirectory/Images/Generated",
            "--static-members",
            "true",
            "--extension-name",
            "AppUIImage",
            "--extension-name-ui-kit",
            "AppImage",
            "--extension-suffix",
            "Testing",
        ]

        XCTAssertEqual(arguments, expectedArguments)
    }
}

