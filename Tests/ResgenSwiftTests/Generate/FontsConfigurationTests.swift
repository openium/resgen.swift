//
//  FontsConfigurationTests.swift
//  
//
//  Created by Thibaut Schmitt on 04/11/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class FontsConfigurationTests: XCTestCase {
    
    let projectDirectory = "projectDirectory/"
    
    func test_argsGeneration_requiredArgs() {
        // Given
        let testingConfiguration = FontsConfiguration(inputFile: "path/to/fonts.txt",
                                                      extensionOutputPath: "Fonts/Generated",
                                                      extensionName: nil,
                                                      extensionNameUIKit: nil,
                                                      extensionSuffix: nil,
                                                      infoPlistPaths: nil,
                                                      staticMembers: nil)
        // When
        let arguments = testingConfiguration.getArguments(projectDirectory: projectDirectory, force: false)
                
        // Expect
        let expectedArguments = [
            "projectDirectory/path/to/fonts.txt",
            "--extension-output-path",
            "projectDirectory/Fonts/Generated",
            "--static-members",
            "false"
        ]
        
        XCTAssertEqual(arguments, expectedArguments)
    }
    
    func test_argsGeneration_allArguments() {
        // Given
        let testingConfiguration = FontsConfiguration(inputFile: "path/to/fonts.txt",
                                                      extensionOutputPath: "Fonts/Generated",
                                                      extensionName: "AppUIFont",
                                                      extensionNameUIKit: "AppFont",
                                                      extensionSuffix: "Testing",
                                                      infoPlistPaths: "path/to/plist1.plist path/to/plist2.plist",
                                                      staticMembers: true)
        // When
        let arguments = testingConfiguration.getArguments(projectDirectory: projectDirectory, force: true)
                
        // Expect
        let expectedArguments = [
            "-f",
            "projectDirectory/path/to/fonts.txt",
            "--extension-output-path",
            "projectDirectory/Fonts/Generated",
            "--static-members",
            "true",
            "--extension-name",
            "AppUIFont",
            "--extension-name-ui-kit",
            "AppFont",
            "--extension-suffix",
            "Testing",
            "--info-plist-paths",
            "projectDirectory/path/to/plist1.plist projectDirectory/path/to/plist2.plist"
        ]
        
        XCTAssertEqual(arguments, expectedArguments)
    }
}
