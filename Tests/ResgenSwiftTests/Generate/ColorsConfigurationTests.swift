//
//  ColorsConfigurationTests.swift
//  
//
//  Created by Thibaut Schmitt on 04/11/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class ColorsConfigurationTests: XCTestCase {
    
    let projectDirectory = "projectDirectory/"
    
    func test_argsGeneration_requiredArgs() {
        // Given
        let testingConfiguration = ColorsConfiguration(inputFile: "path/to/colors.txt",
                                                       style: ColorStyle.all.rawValue,
                                                       xcassetsPath: "path/to/assets.xcassets",
                                                       extensionOutputPath: "Colors/Generated",
                                                       extensionName: nil,
                                                       extensionNameUIKit: nil,
                                                       extensionSuffix: nil,
                                                       staticMembers: false)
        // When
        let arguments = testingConfiguration.getArguments(projectDirectory: projectDirectory, force: false)
                
        // Expect
        let expectedArguments = [
            "projectDirectory/path/to/colors.txt",
            "--style",
            "all",
            "--xcassets-path",
            "projectDirectory/path/to/assets.xcassets",
            "--extension-output-path",
            "projectDirectory/Colors/Generated",
            "--static-members",
            "false"
        ]
        
        XCTAssertEqual(arguments, expectedArguments)
    }
    
    func test_argsGeneration_allArguments() {
        // Given
        let testingConfiguration = ColorsConfiguration(inputFile: "path/to/colors.txt",
                                                       style: ColorStyle.all.rawValue,
                                                       xcassetsPath: "path/to/assets.xcassets",
                                                       extensionOutputPath: "Colors/Generated",
                                                       extensionName: "AppUIColor",
                                                       extensionNameUIKit: "AppColor",
                                                       extensionSuffix: "Testing",
                                                       staticMembers: false)
        // When
        let arguments = testingConfiguration.getArguments(projectDirectory: projectDirectory, force: true)
                
        // Expect
        let expectedArguments = [
            "-f",
            "projectDirectory/path/to/colors.txt",
            "--style",
            "all",
            "--xcassets-path",
            "projectDirectory/path/to/assets.xcassets",
            "--extension-output-path",
            "projectDirectory/Colors/Generated",
            "--static-members",
            "false",
            "--extension-name",
            "AppUIColor",
            "--extension-name-ui-kit",
            "AppColor",
            "--extension-suffix",
            "Testing",
        ]
        
        XCTAssertEqual(arguments, expectedArguments)
    }
}
