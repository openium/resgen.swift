//
//  FontNameTests.swift
//  
//
//  Created by Thibaut Schmitt on 05/09/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class FontNameTests: XCTestCase {
 
    func test_uiKit_GeneratedProperty_noForbiddenCharacter() {
        // Given
        let fontName: FontName = "CircularStdBold"
        
        // When
        let property = fontName.getProperty(isStatic: true, isSwiftUI: false)
        
        // Expect
        let expect = """
         static let CircularStdBold: ((_ size: CGFloat) -> UIFont) = { size in
             UIFont(name: FontName.CircularStdBold.rawValue, size: size)!
         }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_uiKit_GeneratedProperty_withForbiddenCharacter() {
        // Given
        let fontName: FontName = "[Circular_Std+Bold-Underline]"
        
        // When
        let property = fontName.getProperty(isStatic: true, isSwiftUI: false)
        
        // Expect
        let expect = """
         static let CircularStdBoldUnderline: ((_ size: CGFloat) -> UIFont) = { size in
             UIFont(name: FontName.CircularStdBoldUnderline.rawValue, size: size)!
         }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_uiKit_GeneratedMethod_noForbiddenCharacter() {
        // Given
        let fontName: FontName = "CircularStdBold"
        
        // When
        let property = fontName.getProperty(isStatic: false, isSwiftUI: false)
        
        // Expect
        let expect = """
         func CircularStdBold(withSize size: CGFloat) -> UIFont {
             UIFont(name: FontName.CircularStdBold.rawValue, size: size)!
         }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_uiKit_GeneratedMethod_withForbiddenCharacter() {
        // Given
        let fontName: FontName = "[Circular_Std+Bold-Underline]"
        
        // When
        let property = fontName.getProperty(isStatic: false, isSwiftUI: false)
        
        // Expect
        let expect = """
        func CircularStdBoldUnderline(withSize size: CGFloat) -> UIFont {
            UIFont(name: FontName.CircularStdBoldUnderline.rawValue, size: size)!
        }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
 
    func test_swiftUI_GeneratedProperty_noForbiddenCharacter() {
        // Given
        let fontName: FontName = "CircularStdBold"
        
        // When
        let property = fontName.getProperty(isStatic: true, isSwiftUI: true)
        
        // Expect
        let expect = """
         static let CircularStdBold: ((_ size: CGFloat) -> Font) = { size in
             Font.custom(FontName.CircularStdBold.rawValue, size: size)
         }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_swiftUI_GeneratedProperty_withForbiddenCharacter() {
        // Given
        let fontName: FontName = "[Circular_Std+Bold-Underline]"
        
        // When
        let property = fontName.getProperty(isStatic: true, isSwiftUI: true)
        
        // Expect
        let expect = """
         static let CircularStdBoldUnderline: ((_ size: CGFloat) -> Font) = { size in
             Font.custom(FontName.CircularStdBoldUnderline.rawValue, size: size)
         }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_swiftUI_GeneratedMethod_noForbiddenCharacter() {
        // Given
        let fontName: FontName = "CircularStdBold"
        
        // When
        let property = fontName.getProperty(isStatic: false, isSwiftUI: true)
        
        // Expect
        let expect = """
         func CircularStdBold(withSize size: CGFloat) -> Font {
             Font.custom(FontName.CircularStdBold.rawValue, size: size)
         }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
    
    func test_swiftUI_GeneratedMethod_withForbiddenCharacter() {
        // Given
        let fontName: FontName = "[Circular_Std+Bold-Underline]"
        
        // When
        let property = fontName.getProperty(isStatic: false, isSwiftUI: true)
        
        // Expect
        let expect = """
        func CircularStdBoldUnderline(withSize size: CGFloat) -> Font {
            Font.custom(FontName.CircularStdBoldUnderline.rawValue, size: size)
        }
        """
            
        XCTAssertEqual(property.adaptForXCTest(), expect.adaptForXCTest())
    }
}
