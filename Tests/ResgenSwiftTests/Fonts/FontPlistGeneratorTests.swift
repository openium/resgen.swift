//
//  FontPlistGeneratorTests.swift
//  
//
//  Created by Thibaut Schmitt on 05/09/2022.
//

import Foundation
import XCTest

@testable import ResgenSwift

final class FontPlistGeneratorTests: XCTestCase {
    func testGeneratedPlist() {
        // Given
        let fontNames: [FontName] = [
            "CircularStd-Regular",
            "CircularStd-Bold"
        ]
        
        // When
        let plistContent = FontPlistGenerator.generatePlistUIAppsFontContent(for: fontNames, infoPlistPaths: [String]())
        
        // Expect
        let expect = """
        <key>UIAppFonts</key>
            <array>
                <string>CircularStd-Regular</string>
                <string>CircularStd-Bold</string>
            </array>
        """
        
        XCTAssertEqual(plistContent.adaptForXCTest(), expect.adaptForXCTest())
    }
}
