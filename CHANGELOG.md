# v1.2 - Architecture generation

## New
- New section in configuration file: `architecture`. Define your ressources accessors achitecture and let ResgenSwift generate it for you. Check out Readme to know more.

## Fixes
- Errors and Warnings are now shown in Xcode Issue Navigator

---
# v1.1 - SwiftUI compatibility

## New
- Update plist `UIAppFonts` when generated fonts (use plistBuddy)
	- New parameter: `infoPlistPaths`
- Generate SwiftUI extensions for colors, fonts and images
	- New parameter: `extensionNameUIKit`
- Adding Makefile to install, unsintall and create man page.

## Fixes
Fix SwiftLint rule `trailing_newline`

---
# v1.0 - Configuration file

## Major
- A new command has been added: `generate`. Instead of runnning every single command, it will run all necessary command based on a `yaml` configuration file. Check out Readme to know more.

## Minors
- Code refactoring
- Huge performance improvements
- Readme.md update
- Add option to generate static properties/methods (`staticMembers`)
- Add option to specify the project directory (`--project-directory`). It allows to run ResgenSwift from anywhere
- Add `install.sh` script to install ResgenSwift in `/usr/local/bin`
