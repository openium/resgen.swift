SHELL = /bin/zsh

#INSTALL_DIR ?= /usr/local/bin
INSTALL_DIR = /tmp/ResgenYolo

MAN_DIR := /usr/local/share/man
MAN_PAGE_NAME = resgen-swift.1
REPO_DIR = $(shell pwd)
BUILD_DIR = $(REPO_DIR)/.build

#
# Man pages
#

# create-man-files:
# 	swift package plugin generate-manual
# 	cp $(BUILDDIR)/plugins/GenerateManualPlugin/outputs/ResgenSwift/resgen-swift.1 $(REPODIR)/man/resgen-swift.1

# install-man-files:
# 	mkdir -p ${DESTDIR}${mandir}/man1
# 	cp $(REPODIR)/man/resgen-swift.1 ${DESTDIR}${mandir}/man1/resgen-swift.1

create-and-install-man-files:
	swift package plugin generate-manual
	mkdir -p ${MAN_DIR}/man1
	cp $(BUILD_DIR)/plugins/GenerateManualPlugin/outputs/ResgenSwift/${MAN_PAGE_NAME} ${MAN_DIR}/man1/${MAN_PAGE_NAME}

#
# Build and install
#

build-debug:
	@swift build \
		-c debug \
		--build-path "$(BUILD_DIR)"

build-release:
	@swift build \
		-c release \
		--build-path "$(BUILD_DIR)"

.PHONY: install
install: build-release
	@install -d "$(INSTALL_DIR)"
	@install "$(wildcard $(BUILD_DIR)/**/release/ResgenSwift)" "$(INSTALL_DIR)/resgen-swift"
	@make create-and-install-man-files

#
# Uninstall and cleaning
#

.PHONY: uninstall
uninstall:
	@rm -rf "$(INSTALL_DIR)/resgen-swift"
	@rm -rf ${MAN_DIR}/man1/${MAN_PAGE_NAME}

.PHONY: clean
distclean:
	@rm -f $(BUILD_DIR)/release
	
.PHONY: clean
clean: distclean
	@rm -rf $(BUILD_DIR)
