#/bin/bash

FORCE_FLAG="$1"

## Font
#swift run -c release ResgenSwift fonts $FORCE_FLAG "./Fonts/sampleFontsAll.txt" \
# --extension-output-path "./Fonts/Generated" \
# --extension-name "FontYolo" \
# --extension-name-ui-kit "UIFontYolo" \
# --extension-suffix "GenAllScript" \
# --info-plist-paths "./Fonts/Generated/test.plist ./Fonts/Generated/test2.plist"
#
#echo "\n-------------------------\n"
#
## Color
#swift run -c release ResgenSwift colors $FORCE_FLAG "./Colors/sampleColors1.txt" \
# --style all \
# --xcassets-path "./Colors/colors.xcassets" \
# --extension-output-path "./Colors/Generated/" \
# --extension-name "ColorYolo" \
# --extension-name-ui-kit "UIhkjhkColorYolo" \
# --extension-suffix "GenAllScript"
#
#echo "\n-------------------------\n"
#
## Twine
#swift run -c release ResgenSwift strings twine $FORCE_FLAG "./Twine/sampleStrings.txt" \
# --output-path "./Twine/Generated" \
# --langs "fr en en-us" \
# --default-lang "en" \
# --extension-output-path "./Twine/Generated"

#echo "\n-------------------------\n"

## Strings
#swift run -c release ResgenSwift strings stringium $FORCE_FLAG "./Strings/sampleStrings.txt" \
# --output-path "./Strings/Generated" \
# --langs "fr en en-us" \
# --default-lang "en" \
# --extension-output-path "./Strings/Generated" \
# --extension-name "String" \
# --extension-suffix "GenAllScript"

#echo "\n-------------------------\n"

## Tags
#swift run -c release ResgenSwift strings tags $FORCE_FLAG "./Tags/sampleTags.txt" \
# --lang "ium" \
# --extension-output-path "./Tags/Generated" \
# --extension-name "Tags" \
# --extension-suffix "GenAllScript"

#echo "\n-------------------------\n"

# Analytics
swift run -c release ResgenSwift analytics $FORCE_FLAG "./Tags/sampleTags.yml" \
 --target "matomo firebase" \
 --extension-output-path "./Tags/Generated" \
 --extension-name "Analytics" \
 --extension-suffix "GenAllScript"

#echo "\n-------------------------\n"
#
## Images
#swift run -c release ResgenSwift images $FORCE_FLAG "./Images/sampleImages.txt" \
# --xcassets-path "./Images/imagium.xcassets" \
# --extension-output-path "./Images/Generated" \
# --extension-name "ImageYolo" \
# --extension-name-ui-kit "UIImageYolo" \
# --extension-suffix "GenAllScript"
