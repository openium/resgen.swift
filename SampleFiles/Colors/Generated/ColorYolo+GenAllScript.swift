// Generated by ResgenSwift.Color 1.2

import SwiftUI

extension ColorYolo {

    /// Color red is #FF0000 (light) or #FF0000 (dark)"
    var red: Color {
        Color("red")
    }

    /// Color green_alpha_50 is #A000FF00 (light) or #A000FF00 (dark)"
    var green_alpha_50: Color {
        Color("green_alpha_50")
    }

    /// Color blue_light_dark is #0000FF (light) or #0000AA (dark)"
    var blue_light_dark: Color {
        Color("blue_light_dark")
    }
}
