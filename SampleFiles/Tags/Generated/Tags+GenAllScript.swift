// Generated by ResgenSwift.Strings.Tags 1.2

import UIKit

extension Tags {

    // MARK: - ScreenTag

    /// Translation in ium :
    /// Ecran un
    var screen_one: String {
        "Ecran un"
    }

    /// Translation in ium :
    /// Ecran deux
    var screen_two: String {
        "Ecran deux"
    }
}
