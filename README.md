# ResgenSwift

ResgenSwift is a package, fully written in Swift, to help you automatize ressource update and generation.

> 🧐 For all commands, see samples files in `SampleFiles`

## Fonts

Font generator generates an extension of `UIFont` and `Font` (or custom classes). It also prints content of `UIAppFonts` from your project `.plist`. If project `.plist` is specified, it will update `UIAppFonts` content of all `.plist`.

iOS required to use the **real name** of the font, this name can be different from its filename. To get the **real name**, it uses `fc-scan`. So, be sure that the `$PATH` contains path of `fc-scan`.

**Example**

```sh
swift run -c release ResgenSwift fonts $FORCE_FLAG "./Fonts/fonts.txt" \
 --extension-output-path "./Fonts/Generated" \
 --extension-name "AppFont" \
 --extension-name-ui-kit "UIAppFont" \
 --extension-suffix "GreatApp" \
 --static-members true \
 --info-plist-paths "./path/one/to/Info.plist ./path/two/to/Info.plist"
```

**Parameters**

1. `-f`: force generation
2. Font input folder, it will search for every `.ttf` and `.otf` files specified in `fonts.txt`
3. `--extension-output-path`: path where to generate generated extension
4. `--extension-name` *(optional)* : name of the class to add SwiftUI getters
5. `--extension-name-ui-kit` *(optional)* : name of the class to add UIKit getters
6. `--extension-suffix` *(optional)* : additional text which is added to the filename (ex: `AppFont+GreatApp.swift`)
7. `--static-members` *(optional)*: generate static properties or not
8. `--info-plist-paths` *(optional)*: array of `.plist`, you can specify multiple `Info.plist` for multiple targets


## Colors

Colors generator generates an extension of `UIColor` (or a custom class) along with colorsets in specified xcassets.

```sh
swift run -c release ResgenSwift colors $FORCE_FLAG "./Colors/colors.txt" \
 --style all \
 --xcassets-path "./Colors/colors.xcassets" \
 --extension-output-path "./Colors/Generated/" \
 --extension-name "AppColor" \
 --extension-name-ui-kit "UIAppColor" \ 
 --extension-suffix "GreatApp" \
 --static-members true
```

**Parameters**

1. `-f`: force generation
2. Input colors file
3. `--style` can be `all` or `light`
4. `--extension-output-path`: path where to generate generated extension
5. `--extension-name` *(optional)* : name of the class to add SwiftUI getters
6. `--extension-name-ui-kit` *(optional)* : name of the class to add UIKit getters
7. `--extension-suffix` *(optional)* : additional text which is added to filename (ex: `AppColor+GreatApp.swift`)
8. `--static-members` *(optional)*: generate static properties or not
   
 
## Strings

Strings command allows to generate `strings` files along with extensions to access those strings easily. It can do it 2 ways: Twine and Stringium. It is not recommended to use Twine except on legacy projects or while migrating to ResgenSwift, because it use https://github.com/openium/twine. Using Stringium is recommended because it does not required external dependency and allow more customisation.

### Twine (not recommended)

```sh
swift run -c release ResgenSwift strings twine $FORCE_FLAG "./Twine/strings.txt" \
 --output-path "./Twine/Generated" \
 --langs "fr en en-us" \
 --default-lang "en" \
 --extension-output-path "./Twine/Generated"
```
 
 **Parameters**

1. `-f`: force generation
2. Input translations file (must be Twine formatted)
3. `--langs`: langs to generate (string with space between each lang)
4. `--default-lang`: default lang that will be in `Base.lproj`. It must be in `langs` as well
5. `--extension-output-path`: path where to generate generated extension

### Stringium (recommended)

```sh
swift run -c release ResgenSwift strings stringium $FORCE_FLAG "./Strings/strings.txt" \
 --output-path "./Strings/Generated" \
 --langs "fr en en-us" \
 --default-lang "en" \
 --extension-output-path "./Strings/Generated" \
 --extension-name "AppString" \
 --extension-suffix "GreatApp" \
 --xcStrings true
 --static-members true
```

 **Parameters**

1. `-f`: force generation
2. Input translations file (must be Twine formatted)
3. `--langs`: langs to generate (string with space between each lang)
4. `--default-lang`: default lang that will be in `Base.lproj`. It must be in `langs` as well
4. `--extension-output-path`: path where to generate generated extension
5. `--extension-name` *(optional)* : name of class to add the extension
6. `--extension-suffix` *(optional)* : additional text which is added to filename (ex: `AppString+GreatApp.swift`)
6. `--xcStrings`*(optional)* : generate string catalog
7. `--static-members` *(optional)*: generate static properties or not


## Tags

Tags is also a subcommand of `Strings`. Input files are formatted the same way. Tags will generate properties which return exactly what is specified in the input file. It was designed to be used for analytics purpose and to be shared with any other platform to have the same analytics keys.

```sh
swift run -c release ResgenSwift strings tags $FORCE_FLAG "./Tags/tags.txt" \
 --lang "ium" \
 --extension-output-path "./Tags/Generated" \
 --extension-name "AppTags" \
 --extension-suffix "GreatApp" \
 --static-members true
```

 **Parameters**

1. `-f`: force generation
2. Input tags file (must be Twine formatted)
3. `--lang`: langs to look at in input file
4. `--extension-output-path`: path where to generate generated extension
5. `--extension-name` *(optional)* : name of class to add the extension
6. `--extension-suffix` *(optional)* : additional text which is added to filename (ex: `AppTags+GreatApp.swift`)
7. `--static-members` *(optional)*: generate static properties or not

> ⚠️ If extension name is not set or is `Tags`, it will generate the following typaloas  `typealias Tags = String`.


## Analytics

Analytics will generate all you need to analyze UX with Matomo or Firebase Analytics. Input files are formatted in YAML. This command will generate a manager for each target and an AnalyticsManager. This is this one you will need to use. And it will generate a method for all tags you have declared in the YAML file. Next, you will need to use the `configure()` method of AnalyticsManager and if you want to use matomo to set up the `siteId` and the `url` of the site.

```sh
swift run -c release ResgenSwift strings tags $FORCE_FLAG "./Tags/tags.txt" \
 --target "matomo firebase" \
 --extension-output-path "./Analytics/Generated" \
 --extension-name "AppAnalytics" \
 --extension-suffix "GreatApp" \
 --static-members true
```
 
 **Parameters**

1. `-f`: force generation
2. Input tags file (must be YAML formatted)
3. `--target`: target with you will log UX
4. `--extension-output-path`: path where to generate generated extension
5. `--extension-name` *(optional)* : name of class to add the extension
6. `--extension-suffix` *(optional)* : additional text which is added to filename (ex: `AppAnalytics+GreatApp.swift`)
7. `--static-members` *(optional)*: generate static properties or not

> ⚠️ If extension name is not set or is `Analytics`, it will generate the following typaloas  `typealias Analytics = String`.

### YAML

```
 - id: s1_def_one
   name: s1 def one _TITLE_
   path: s1_def_one/_TITLE_
   action: Tap
   category: User
   tags: ios,droid
   comments:
   parameters:
     - name: title
       type: String
       replaceIn: name,path
```

1. `id`: name of the method (method name will be composed of `log` + `Event|Screen` + `id`)
2. `name`: name of the tag
3. `path` *(optional with firebase)* : needed for matomo but not with firebase (log screen)
4. `action` *(optional with firebase)* : needed for matomo but not with firebase (log event)
5. `category` *(optional with firebase)* : needed for matomo but not with firebase (log event)
6. `tags`: which platform target
7. `comments` *(optional)*
8. `parameters` *(optional)*

 **Parameters**

You can use parameters in generate methods. 

1. `name`: name of the parameter
2. `type`: type of the parameter (Int, String, Bool, Double)
3. `replaceIn` *(optional)*

**Replace in**

This is section is equivalent of `%s | %d | %f | %@`. You can put the content of the parameter in *name*, *path*, *action*, *category*.
You need to put `_` + `NAME OF THE PARAMETER` + `_` in the target and which target you want in the value of `replaceIn`. (name need to be in uppercase)


## Images

Images generator will generate images assets along with extensions to access those images easily. 

```sh
swift run -c release ResgenSwift images $FORCE_FLAG "./Images/images.txt" \
 --xcassets-path "./Images/app.xcassets" \
 --extension-output-path "./Images/Generated" \
 --extension-name "AppImage" \
 --extension-name-ui-kit "UIAppImage" \ 
 --extension-suffix "GreatApp" \
 --static-members true
```

 **Parameters**

1. `-f`: force generation
2. Input images definitions file
3. `--xcassets-path`: xcasset path where to generate imagesets
4. `--extension-output-path`: path where to generate generated extension
5. `--extension-name` *(optional)* : name of the class to add SwiftUI getters
6. `--extension-name-ui-kit` *(optional)* : name of the class to add UIKit getters
6. `--extension-suffix` *(optional)* : additional text which is added to filename (ex: `AppImage+GreatApp.swift`)
7. `--static-members` *(optional)*: generate static properties or not

> ⚠️ Svg images will be copied in the assets and rendered as "Original", however if those images are not rendered correctly you can force the png generation by adding the key word "png" like this: id arrow_back 15 ? png

## All at once

Another command exists to generate all ressources at the same time: `generate`. It use the following commands: `Fonts`, `Colors`, `Strings/Stringium`, `Strings/Tags`, `Images`.

All parameters can be specified in a configuration file in `Yaml`:

> Order of configuration types does not matter. Order them to fit your needs.

```yaml
---
colors:
-
  inputFile: String
  style: [light/all]
  xcassetsPath: String
  extensionOutputPath: String
  extensionName: String?
  extensionNameUIKit: String?
  extensionSuffix: String?
  staticMembers: Bool?

fonts:
-
  inputFile: String
  extensionOutputPath: String
  extensionName: String?
  extensionNameUIKit: String?
  extensionSuffix: String?
  staticMembers: Bool?
  infoPlistPaths: [String]
  
images:
- 
  inputFile: String
  xcassetsPath: String
  extensionOutputPath: String
  extensionName: String?
  extensionNameUIKit: String?
  extensionSuffix: String?
  staticMembers: Bool?

strings:
- 
  inputFile: String
  outputPath: String
  langs: String
  defaultLang: String
  extensionOutputPath: String
  extensionName: String?
  extensionSuffix: String?
  staticMembers: Bool?

tags:
- 
  inputFile: String
  lang: String
  extensionOutputPath: String
  extensionName: String?
  extensionSuffix: String?
  staticMembers: Bool?
```

### Multiple configurations

In some case, you may need to have 2 colors files in your projects. You will need 2 colors configurations. Every configuration type is an array and can contains as many configurations as you need. 

Sample for 2 colors configurations:

```yaml
...
colors:
-
  inputFile: String
  style: [light/all]
  xcassetsPath: String
  extensionOutputPath: String
  extensionName: String?
  extensionNameUIKit: String?
  extensionSuffix: String?
  staticMembers: Bool?
-
  inputFile: String
  style: [light/all]
  xcassetsPath: String
  extensionOutputPath: String
  extensionName: String?
  extensionNameUIKit: String?
  extensionSuffix: String?
  staticMembers: Bool?  
...
```

### No configuration

In some case, you may not need to generate tags for example. You must specified `tags` as an empty array :

```yaml
...
tags: []
...
```

### File architecture

ResgenSwift generate extension of classes. Those classes must exists in your project. You can create them yourself OR you can let ResgenSwift create them by specifying what you want. Do as follow: 

```
architecture:
  property: R *(required but not used)*
  classname: R
  path: ./path/to/generate
  children:
    - property: images
      classname: R2Image
    - property: strings
      classname: R2String
    - property: fonts
      classname: R2Font
    - property: images
      classname: R2Image
    - property: uikit
      classname: R2UI
      children:
        - property: images
          classname: R2UIImage
        - property: fonts
          classname: R2UIFont
        - property: images
          classname: R2UIImage
```

This will generate a file named as the architecture classname: `R.swift`. Based on the previous architecture, it will generate:
```
class R {
    static let images = R2Image()
    static let strings = R2String()
    static let fonts = R2Font()
    static let images = R2Image()
    static let uikit = R2UI()
}

class R2Image {}

class R2String {}

class R2Font {}

class R2Image {}

class R2UI {
    let images = R2UIImage()
    let fonts = R2UIFont()
    let images = R2UIImage()
}

class R2UIImage {}

class R2UIFont {}

class R2UIImage {}
```

          
### Usage

```sh
swift run -c release ResgenSwift generate path/to/configuration.yml --project-directory ${PROJECT_DIR}
```

> ⚠️ Every path in `configuration.yml` will be prepended by content of `--project-directory` if they are relative path (not starting with `/`)


## Binary usage

### Installation

Run `make install`. Binary will be install in `/usr/local/bin`.

Usage:

```sh
resgen-swift generate path/to/configuration.yml --project-directory ${PROJECT_DIR}
```

### Man page

Commands parameters and details can be find by running `resgen-swift --help`. If you prefer, a man page is also available. Run `man resgen-swift`. Man page is installed on `make install` but you can install manually by running `make create-and-install-man-file`.

### Uninstallation

To uninstall ResgenSwift: `make uninstall`.
