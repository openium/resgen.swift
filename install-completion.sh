#!/bin/zsh

if [[ $SHELL == "/bin/zsh" ]]
then
    if [ ! -d "~/.oh-my-zsh/completions" ]
    then
        mkdir -p ~/.oh-my-zsh/completions
    fi
    resgen-swift --generate-completion-script zsh > ~/.oh-my-zsh/completions/_resgen-swift
else
    echo "Completion file can only be installed automaticaly for oh-my-zsh shell. See https://apple.github.io/swift-argument-parser/documentation/argumentparser/installingcompletionscripts to install it ti your shell"
fi

